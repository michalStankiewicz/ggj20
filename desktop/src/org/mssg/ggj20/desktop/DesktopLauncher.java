package org.mssg.ggj20.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.assets.CFG;

public class DesktopLauncher {
	public static void main (String[] arg) {
		CFG.loadConfig();

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.width = CFG.getInt("resolution.width");
		config.height = CFG.getInt("resolution.height");
		config.fullscreen = CFG.getBoolean("fullscreenMode");
		config.forceExit = true;

		new LwjglApplication(new GGJ20(), config);
	}
}
