package org.mssg.ggj20.assets;

import com.badlogic.gdx.assets.loaders.I18NBundleLoader.I18NBundleParameter;
import com.badlogic.gdx.utils.I18NBundle;

import java.util.Locale;

class AssetsI18NInitializer extends AssetsLoader {

    private static final String I18N_LANGUAGE_FILE = "i18n/language";

    @Override
    protected void loadAssets() {
        I18NBundleParameter params = new I18NBundleParameter(Locale.forLanguageTag(CFG.CONFIG.getProperty("language")));
        assetManager.load(I18N_LANGUAGE_FILE, I18NBundle.class, params);
    }

    @Override
    protected void initAssets() {
        I18N.LANGUAGE = assetManager.get(I18N_LANGUAGE_FILE, I18NBundle.class);
        I18NBundle.setExceptionOnMissingKey(false);
    }

    @Override
    protected void disposeAssets() {
    }
}
