package org.mssg.ggj20.assets;

import com.badlogic.gdx.maps.tiled.TiledMap;

public class MAPSInitializer extends AssetsLoader {

    public static final String MAP_FILE = "levels/office.tmx";

    @Override
    protected void loadAssets() {
        assetManager.load(MAP_FILE, TiledMap.class);
    }

    @Override
    protected void initAssets() {
        MAPS.MAP = assetManager.get(MAP_FILE, TiledMap.class);
    }

    @Override
    protected void disposeAssets() {

    }
}
