package org.mssg.ggj20.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;

public class UI {

    public static Skin SHADE_SKIN;
    public static TextureAtlas SHADE_ATLAS;
}
