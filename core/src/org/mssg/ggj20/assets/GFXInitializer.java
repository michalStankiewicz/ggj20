package org.mssg.ggj20.assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class GFXInitializer extends AssetsLoader {
    @Override
    protected void loadAssets() {
        assetManager.load("gfx/tutorial/tutorial.png", Texture.class);

        assetManager.load("gfx/players/raptor/still.png", Texture.class);
        assetManager.load("gfx/players/octo/still.png", Texture.class);
        assetManager.load("gfx/players/jar/still.png", Texture.class);

        assetManager.load("gfx/clouds/problem.png", Texture.class);
        assetManager.load("gfx/clouds/donut.png", Texture.class);
        assetManager.load("gfx/clouds/bug.png", Texture.class);
        assetManager.load("gfx/clouds/computer.png", Texture.class);

        assetManager.load("gfx/clouds/happy1.png", Texture.class);
        assetManager.load("gfx/clouds/happy2.png", Texture.class);
        assetManager.load("gfx/clouds/happy3.png", Texture.class);

        assetManager.load("gfx/players/raptor/raptor_back.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/raptor/raptor_front.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/raptor/raptor_left.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/raptor/raptor_right.atlas", TextureAtlas.class);

        assetManager.load("gfx/players/octo/octo_back.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/octo/octo_front.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/octo/octo_left.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/octo/octo_right.atlas", TextureAtlas.class);

        assetManager.load("gfx/players/jar/jar_back.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/jar/jar_front.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/jar/jar_left.atlas", TextureAtlas.class);
        assetManager.load("gfx/players/jar/jar_right.atlas", TextureAtlas.class);

        assetManager.load("gfx/arrows/generic.png", Texture.class);
        assetManager.load("gfx/arrows/jar_needed.png", Texture.class);
        assetManager.load("gfx/arrows/octo_needed.png", Texture.class);
        assetManager.load("gfx/arrows/raptor_needed.png", Texture.class);

        assetManager.load("gfx/bg/bsod.png", Texture.class);

        assetManager.load("gfx/itGuys/babe.png", Texture.class);
        assetManager.load("gfx/itGuys/babe 2.png", Texture.class);
        assetManager.load("gfx/itGuys/dynia 1.png", Texture.class);
        assetManager.load("gfx/itGuys/dynia 2.png", Texture.class);
        assetManager.load("gfx/itGuys/foka 1.png", Texture.class);
        assetManager.load("gfx/itGuys/foka 2.png", Texture.class);
        assetManager.load("gfx/itGuys/kosmonauta 1.png", Texture.class);
        assetManager.load("gfx/itGuys/kosmonauta 2.png", Texture.class);
        assetManager.load("gfx/itGuys/kotek 1.png", Texture.class);
        assetManager.load("gfx/itGuys/kotek 2.png", Texture.class);
        assetManager.load("gfx/itGuys/poduszka 1.png", Texture.class);
        assetManager.load("gfx/itGuys/poduszka 2.png", Texture.class);

        assetManager.load("gfx/faces/happy.png",Texture.class);
        assetManager.load("gfx/faces/less_happy.png",Texture.class);
        assetManager.load("gfx/faces/worried.png",Texture.class);
        assetManager.load("gfx/faces/angry.png",Texture.class);
        assetManager.load("gfx/faces/furious.png",Texture.class);

        assetManager.load("gfx/ui/bg.png", Texture.class);
    }

    @Override
    protected void initAssets() {
        GFX.TUTORIAL = assetManager.get("gfx/tutorial/tutorial.png");

        GFX.DONUT_RAPTOR_STILL = assetManager.get("gfx/players/raptor/still.png", Texture.class);
        GFX.OCTO_PIE_STILL = assetManager.get("gfx/players/octo/still.png", Texture.class);
        GFX.COOKIE_JAR_STILL = assetManager.get("gfx/players/jar/still.png", Texture.class);

        GFX.THANK_YOU = new Texture[3];
        GFX.THANK_YOU[0] = assetManager.get("gfx/clouds/happy1.png", Texture.class);
        GFX.THANK_YOU[1] = assetManager.get("gfx/clouds/happy2.png", Texture.class);
        GFX.THANK_YOU[2] = assetManager.get("gfx/clouds/happy3.png", Texture.class);

        GFX.QUESTION_MARK = assetManager.get("gfx/clouds/problem.png", Texture.class);
        GFX.DONUT = assetManager.get("gfx/clouds/donut.png", Texture.class);
        GFX.BUG = assetManager.get("gfx/clouds/bug.png", Texture.class);
        GFX.NO_POWER = assetManager.get("gfx/clouds/computer.png", Texture.class);

        GFX.RAPTOR_BACK = assetManager.get("gfx/players/raptor/raptor_back.atlas", TextureAtlas.class);
        GFX.OCTO_BACK = assetManager.get("gfx/players/octo/octo_back.atlas", TextureAtlas.class);
        GFX.JAR_BACK = assetManager.get("gfx/players/jar/jar_back.atlas", TextureAtlas.class);

        GFX.RAPTOR_FRONT = assetManager.get("gfx/players/raptor/raptor_front.atlas", TextureAtlas.class);
        GFX.OCTO_FRONT = assetManager.get("gfx/players/octo/octo_front.atlas", TextureAtlas.class);
        GFX.JAR_FRONT = assetManager.get("gfx/players/jar/jar_front.atlas", TextureAtlas.class);

        GFX.RAPTOR_LEFT = assetManager.get("gfx/players/raptor/raptor_left.atlas", TextureAtlas.class);
        GFX.OCTO_LEFT = assetManager.get("gfx/players/octo/octo_left.atlas", TextureAtlas.class);
        GFX.JAR_LEFT = assetManager.get("gfx/players/jar/jar_left.atlas", TextureAtlas.class);

        GFX.RAPTOR_RIGHT = assetManager.get("gfx/players/raptor/raptor_right.atlas", TextureAtlas.class);
        GFX.OCTO_RIGHT = assetManager.get("gfx/players/octo/octo_right.atlas", TextureAtlas.class);
        GFX.JAR_RIGHT = assetManager.get("gfx/players/jar/jar_right.atlas", TextureAtlas.class);

        GFX.HELP_GENERIC_POINTER = assetManager.get("gfx/arrows/generic.png", Texture.class);
        GFX.JAR_NEEDED_POINTER = assetManager.get("gfx/arrows/jar_needed.png", Texture.class);
        GFX.OCTO_NEEDED_POINTER = assetManager.get("gfx/arrows/octo_needed.png", Texture.class);
        GFX.RAPTOR_NEEDED_POINTER = assetManager.get("gfx/arrows/raptor_needed.png", Texture.class);

        GFX.IT_GUYS = new Texture[6];
        GFX.IT_GUYS_PROBLEM = new Texture[6];

        GFX.IT_GUYS[0] = assetManager.get("gfx/itGuys/babe.png", Texture.class);
        GFX.IT_GUYS_PROBLEM[0] = assetManager.get("gfx/itGuys/babe 2.png", Texture.class);
        GFX.IT_GUYS[1] = assetManager.get("gfx/itGuys/dynia 1.png", Texture.class);
        GFX.IT_GUYS_PROBLEM[1] = assetManager.get("gfx/itGuys/dynia 2.png", Texture.class);
        GFX.IT_GUYS[2] = assetManager.get("gfx/itGuys/foka 1.png", Texture.class);
        GFX.IT_GUYS_PROBLEM[2] = assetManager.get("gfx/itGuys/foka 2.png", Texture.class);
        GFX.IT_GUYS[3] = assetManager.get("gfx/itGuys/kosmonauta 1.png", Texture.class);
        GFX.IT_GUYS_PROBLEM[3] = assetManager.get("gfx/itGuys/kosmonauta 2.png", Texture.class);
        GFX.IT_GUYS[4] = assetManager.get("gfx/itGuys/kotek 1.png", Texture.class);
        GFX.IT_GUYS_PROBLEM[4] = assetManager.get("gfx/itGuys/kotek 2.png", Texture.class);
        GFX.IT_GUYS[5] = assetManager.get("gfx/itGuys/poduszka 1.png", Texture.class);
        GFX.IT_GUYS_PROBLEM[5] = assetManager.get("gfx/itGuys/poduszka 2.png", Texture.class);

        GFX.BSOD = assetManager.get("gfx/bg/bsod.png", Texture.class);

        GFX.BOARD_HAPPY = assetManager.get("gfx/faces/happy.png",Texture.class);
        GFX.BOARD_LESS_HAPPY = assetManager.get("gfx/faces/less_happy.png",Texture.class);
        GFX.BOARD_WORRIED = assetManager.get("gfx/faces/worried.png",Texture.class);
        GFX.BOARD_ANGRY = assetManager.get("gfx/faces/angry.png",Texture.class);
        GFX.BOARD_FURIOUS = assetManager.get("gfx/faces/furious.png",Texture.class);

        GFX.BACKGROUND = assetManager.get("gfx/ui/bg.png", Texture.class);
    }

    @Override
    protected void disposeAssets() {

    }
}
