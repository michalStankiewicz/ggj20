package org.mssg.ggj20.assets;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

public class GFX {

    public static Texture TUTORIAL;

    public static Texture DONUT_RAPTOR_STILL;
    public static Texture OCTO_PIE_STILL;
    public static Texture COOKIE_JAR_STILL;

    public static Texture QUESTION_MARK;
    public static Texture DONUT;
    public static Texture[] THANK_YOU;
    public static Texture BUG;
    public static Texture NO_POWER;

    public static TextureAtlas RAPTOR_FRONT;
    public static TextureAtlas RAPTOR_BACK;
    public static TextureAtlas RAPTOR_LEFT;
    public static TextureAtlas RAPTOR_RIGHT;

    public static TextureAtlas OCTO_FRONT;
    public static TextureAtlas OCTO_BACK;
    public static TextureAtlas OCTO_LEFT;
    public static TextureAtlas OCTO_RIGHT;

    public static TextureAtlas JAR_FRONT;
    public static TextureAtlas JAR_BACK;
    public static TextureAtlas JAR_LEFT;
    public static TextureAtlas JAR_RIGHT;

    public static Texture HELP_GENERIC_POINTER;
    public static Texture JAR_NEEDED_POINTER;
    public static Texture OCTO_NEEDED_POINTER;
    public static Texture RAPTOR_NEEDED_POINTER;

    public static Texture[] IT_GUYS;
    public static Texture[] IT_GUYS_PROBLEM;

    public static Texture BSOD;

    public static Texture BOARD_HAPPY;
    public static Texture BOARD_LESS_HAPPY;
    public static Texture BOARD_WORRIED;
    public static Texture BOARD_ANGRY;
    public static Texture BOARD_FURIOUS;
    public static Texture BACKGROUND;
}
