package org.mssg.ggj20.assets;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Disposable;

import java.util.HashMap;
import java.util.Map;

public abstract class AssetsLoader implements Disposable {
    static final AssetManager assetManager = new AssetManager();
    private static Map<Class<?>, AssetsLoader> assetLoaders;

    static {
        assetManager.setLoader(TiledMap.class,
                new TmxMapLoader(new InternalFileHandleResolver()));
    }

    public static void loadAllAssets() {
        assetLoaders = new HashMap<>();
        register(AssetsUIInitializer.class, new AssetsUIInitializer());
        register(AssetsI18NInitializer.class, new AssetsI18NInitializer());
        register(GFXInitializer.class, new GFXInitializer());
        register(MAPSInitializer.class, new MAPSInitializer());
        register(SNDInitializer.class, new SNDInitializer());
    }

    private static void register(Class<?> assetsLoaderClass, AssetsLoader assetsLoader) {
        assetLoaders.put(assetsLoaderClass, assetsLoader);
        assetsLoader.loadAssets();
    }

    public static void disposeAllAssets() {
        for (Map.Entry<Class<?>, AssetsLoader> assetsLoaderEntry :
                assetLoaders.entrySet()) {
            assetsLoaderEntry.getValue().dispose();
        }
    }

    public static boolean isLoadingFinished() {
        return assetManager.update();
    }

    public static int getProgressPrc() {
        return (int) (assetManager.getProgress() * 100);
    }

    public static void initializeAssets() {
        for (Map.Entry<Class<?>, AssetsLoader> assetsLoaderEntry :
                assetLoaders.entrySet()) {
            assetsLoaderEntry.getValue().initAssets();
        }
    }

    @Override
    public final void dispose() {
        disposeAssets();
        assetManager.dispose();
    }

    protected abstract void loadAssets();

    protected abstract void initAssets();

    protected abstract void disposeAssets();
}
