package org.mssg.ggj20.assets;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

public class CFG {

    static Properties CONFIG;

    public static int getInt(String key) {
        return Integer.parseInt(CONFIG.getProperty(key));
    }

    public static void loadConfig() {
        CFG.CONFIG = new Properties();
        try {
            CFG.CONFIG.load(new FileInputStream("config.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        CFG.CONFIG.put("runtimePath", Paths.get(".").toAbsolutePath().normalize().toString());
    }

    public static boolean getBoolean(String key) {
        return Boolean.parseBoolean(CONFIG.getProperty(key));
    }

    public static float getFloat(String key) {
        return Float.parseFloat(CONFIG.getProperty(key));
    }
}
