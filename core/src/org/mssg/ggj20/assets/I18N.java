package org.mssg.ggj20.assets;

import com.badlogic.gdx.utils.I18NBundle;

public class I18N {
    static I18NBundle LANGUAGE;

    public static String get(String key) {
        return LANGUAGE.get(key);
    }
}
