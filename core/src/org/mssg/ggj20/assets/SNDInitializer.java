package org.mssg.ggj20.assets;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SNDInitializer extends AssetsLoader {

    @Override
    protected void loadAssets() {
        assetManager.load("music/Vexento - Pixel Party.mp3", Music.class);

        assetManager.load("sound/ding.mp3", Sound.class);

        assetManager.load("sound/keyboard.mp3", Sound.class);
        assetManager.load("sound/nom.wav", Sound.class);
        assetManager.load("sound/computer.wav", Sound.class);

//        assetManager.load("sound/ding.mp3", Sound.class);
    }

    @Override
    protected void initAssets() {
        SND.MUSIC = assetManager.get("music/Vexento - Pixel Party.mp3", Music.class);
        SND.MUSIC.setLooping(true);

        SND.NEW_PROBLEM = assetManager.get("sound/ding.mp3", Sound.class);

        SND.JAR_NEEDED = assetManager.get("sound/ding.mp3", Sound.class);
        SND.OCTO_NEEDED = assetManager.get("sound/ding.mp3", Sound.class);
        SND.RAPTOR_NEEDED = assetManager.get("sound/ding.mp3", Sound.class);

        SND.COMPUTER_PROBLEM_FIXED = assetManager.get("sound/computer.wav", Sound.class);
        SND.BUG_PROBLEM_FIXED = assetManager.get("sound/keyboard.mp3", Sound.class);
        SND.DONUT_PROBLEM_FIXED = assetManager.get("sound/nom.wav", Sound.class);

    }

    @Override
    protected void disposeAssets() {

    }
}
