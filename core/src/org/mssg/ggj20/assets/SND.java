package org.mssg.ggj20.assets;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

public class SND {
    public static Music MUSIC;

    public static Sound NEW_PROBLEM;
    public static Sound JAR_NEEDED;
    public static Sound OCTO_NEEDED;
    public static Sound RAPTOR_NEEDED;
    public static Sound COMPUTER_PROBLEM_FIXED;
    public static Sound BUG_PROBLEM_FIXED;
    public static Sound DONUT_PROBLEM_FIXED;
    private static boolean enabled = true;

    public static void on() {
        enabled = true;
    }

    public static void off() {
        enabled = false;
    }

    public static void play(Sound sound) {
        if (enabled) {
            sound.play();
        }
    }
}
