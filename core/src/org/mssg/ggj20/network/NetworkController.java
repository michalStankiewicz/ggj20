package org.mssg.ggj20.network;


import org.mssg.ggj20.game.BoardMood;
import org.mssg.ggj20.game.HelpDetails;
import org.mssg.ggj20.game.Player;
import org.mssg.ggj20.network.common.SysOutHandler;
import org.mssg.ggj20.network.connector.ConnectorClientController;
import org.mssg.ggj20.network.connector.ConnectorServerController;
import org.mssg.ggj20.network.connector.handlers.client.InvalidPasswordHandler;
import org.mssg.ggj20.network.connector.handlers.client.PlayerInvitationHandler;
import org.mssg.ggj20.network.connector.handlers.server.ConnectionInitHandler;
import org.mssg.ggj20.network.connector.objects.ConnectionInitEvent;
import org.mssg.ggj20.network.connector.objects.InvalidPasswordEvent;
import org.mssg.ggj20.network.connector.objects.Ping;
import org.mssg.ggj20.network.connector.objects.PlayerInvitationEvent;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.BoardMoodUpdateEvent;
import org.mssg.ggj20.network.game.events.ChatEvent;
import org.mssg.ggj20.network.game.events.GameOverEvent;
import org.mssg.ggj20.network.game.events.GameReadyEvent;
import org.mssg.ggj20.network.game.events.ItGuyNeedsHelpEvent;
import org.mssg.ggj20.network.game.events.LaunchGameEvent;
import org.mssg.ggj20.network.game.events.LevelUpEvent;
import org.mssg.ggj20.network.game.events.LoadedGameEvent;
import org.mssg.ggj20.network.game.events.PlayerAskedToHelpEvent;
import org.mssg.ggj20.network.game.events.PlayerCharacterChangedEvent;
import org.mssg.ggj20.network.game.events.PlayerInputEvent;
import org.mssg.ggj20.network.game.events.PlayerJoinConfirmEvent;
import org.mssg.ggj20.network.game.events.PlayerJoinEvent;
import org.mssg.ggj20.network.game.events.PlayerLoadedEvent;
import org.mssg.ggj20.network.game.events.PlayerReadyChangeEvent;
import org.mssg.ggj20.network.game.events.PlayerSendMessageEvent;
import org.mssg.ggj20.network.game.events.PlayerSolvedProblem;
import org.mssg.ggj20.network.game.events.PlayerUpdateEvent;
import org.mssg.ggj20.network.game.events.PlayersListEvent;
import org.mssg.ggj20.network.game.events.Pong;
import org.mssg.ggj20.network.game.events.ServerFullEvent;
import org.mssg.ggj20.network.game.events.TickReceivedEvent;
import org.mssg.ggj20.network.game.events.TickUpdateEvent;
import org.mssg.ggj20.network.game.handlers.client.BoardMoodUpdateEventHandler;
import org.mssg.ggj20.network.game.handlers.client.ChatHandler;
import org.mssg.ggj20.network.game.handlers.client.GameOverHandler;
import org.mssg.ggj20.network.game.handlers.client.GameReadyHandler;
import org.mssg.ggj20.network.game.handlers.client.ItGuyNeedsHelpEventHandler;
import org.mssg.ggj20.network.game.handlers.client.LaunchGameEventHandler;
import org.mssg.ggj20.network.game.handlers.client.LevelUpEventHandler;
import org.mssg.ggj20.network.game.handlers.client.PingHandler;
import org.mssg.ggj20.network.game.handlers.client.PlayerAskedToHelpEventHandler;
import org.mssg.ggj20.network.game.handlers.client.PlayerJoinConfirmHandler;
import org.mssg.ggj20.network.game.handlers.client.PlayerSolvedProblemHandler;
import org.mssg.ggj20.network.game.handlers.client.PlayerUpdateHandler;
import org.mssg.ggj20.network.game.handlers.client.PlayersListHandler;
import org.mssg.ggj20.network.game.handlers.client.ServerFullHandler;
import org.mssg.ggj20.network.game.handlers.client.TickUpdateHandler;
import org.mssg.ggj20.network.game.handlers.server.LoadedGameEventHandler;
import org.mssg.ggj20.network.game.handlers.server.PlayerCharacterChangedEventHandler;
import org.mssg.ggj20.network.game.handlers.server.PlayerInputHandler;
import org.mssg.ggj20.network.game.handlers.server.PlayerJoinHandler;
import org.mssg.ggj20.network.game.handlers.server.PlayerLoadedHandler;
import org.mssg.ggj20.network.game.handlers.server.PlayerReadyChangeEventHandler;
import org.mssg.ggj20.network.game.handlers.server.PlayerSendMessageHandler;
import org.mssg.ggj20.network.game.handlers.server.PongHandler;
import org.mssg.ggj20.network.game.handlers.server.TickReceivedHandler;
import org.mssg.ggj20.screens.lobby.PlayerCharacter;

import java.util.BitSet;
import java.util.concurrent.ConcurrentHashMap;

public class NetworkController {

    public static void hostGame(String serverPassword) {
        ConnectorServerController.startConnectorServer(serverPassword);
        GameServerController.startGameServer(serverPassword);
    }

    public static void joinLocal(String playerName, String password) {
        ConnectorClientController.joinLocal(playerName, password);
    }

    public static void prepareNetworkStuff() {
        prepareHandlers();
        registerAdditionalClasses();
    }

    private static void prepareHandlers() {
        MessageHandlers.setDefaultHandler(new SysOutHandler());

        MessageHandlers.registerClientEvent(ConnectionInitEvent.class, new ConnectionInitHandler());
        MessageHandlers.registerClientEvent(PlayerJoinEvent.class, new PlayerJoinHandler());
        MessageHandlers.registerClientEvent(PlayerSendMessageEvent.class, new PlayerSendMessageHandler());
        MessageHandlers.registerClientEvent(PlayerLoadedEvent.class, new PlayerLoadedHandler());
        MessageHandlers.registerClientEvent(Pong.class, new PongHandler());
        MessageHandlers.registerClientEvent(TickReceivedEvent.class, new TickReceivedHandler());
        MessageHandlers.registerClientEvent(PlayerInputEvent.class, new PlayerInputHandler());
        MessageHandlers.registerClientEvent(PlayerCharacterChangedEvent.class, new PlayerCharacterChangedEventHandler());
        MessageHandlers.registerClientEvent(PlayerReadyChangeEvent.class, new PlayerReadyChangeEventHandler());
        MessageHandlers.registerClientEvent(LoadedGameEvent.class, new LoadedGameEventHandler());

        MessageHandlers.registerServerEvent(InvalidPasswordEvent.class, new InvalidPasswordHandler());
        MessageHandlers.registerServerEvent(PlayerInvitationEvent.class, new PlayerInvitationHandler());
        MessageHandlers.registerServerEvent(PlayersListEvent.class, new PlayersListHandler());
        MessageHandlers.registerServerEvent(ChatEvent.class, new ChatHandler());
        MessageHandlers.registerServerEvent(ServerFullEvent.class, new ServerFullHandler());
        MessageHandlers.registerServerEvent(PlayerJoinConfirmEvent.class, new PlayerJoinConfirmHandler());
        MessageHandlers.registerServerEvent(GameReadyEvent.class, new GameReadyHandler());
        MessageHandlers.registerServerEvent(Ping.class, new PingHandler());
        MessageHandlers.registerServerEvent(TickUpdateEvent.class, new TickUpdateHandler());
        MessageHandlers.registerServerEvent(LaunchGameEvent.class, new LaunchGameEventHandler());
        MessageHandlers.registerServerEvent(PlayerUpdateEvent.class, new PlayerUpdateHandler());
        MessageHandlers.registerServerEvent(ItGuyNeedsHelpEvent.class, new ItGuyNeedsHelpEventHandler());
        MessageHandlers.registerServerEvent(PlayerAskedToHelpEvent.class, new PlayerAskedToHelpEventHandler());
        MessageHandlers.registerServerEvent(PlayerSolvedProblem.class, new PlayerSolvedProblemHandler());
        MessageHandlers.registerServerEvent(BoardMoodUpdateEvent.class, new BoardMoodUpdateEventHandler());
        MessageHandlers.registerServerEvent(LevelUpEvent.class, new LevelUpEventHandler());
        MessageHandlers.registerServerEvent(GameOverEvent.class, new GameOverHandler());
    }

    private static void registerAdditionalClasses() {
        NetworkUtil.registerClasses(ConcurrentHashMap.class,
                Player.class,
                PlayerCharacter.class,
                HelpDetails.class,
                byte[].class,
                BitSet.class,
                BoardMood.class);
    }
}
