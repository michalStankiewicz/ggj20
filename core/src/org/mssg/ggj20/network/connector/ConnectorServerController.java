package org.mssg.ggj20.network.connector;

import com.esotericsoftware.kryonet.KryoSerialization;
import com.esotericsoftware.kryonet.Server;

import org.mssg.ggj20.commons.ui.ChatView;
import org.mssg.ggj20.network.NetworkUtil;
import org.mssg.ggj20.network.common.EventListener;

import java.io.IOException;

public class ConnectorServerController {

    private static final ConnectorServerController INSTANCE = new ConnectorServerController();

    private Server connectorServer;
    private String serverPassword;
    private int tcpPort;

    private ConnectorServerController() {
        connectorServer = new Server(1024, 1024, new KryoSerialization());
        connectorServer.addListener(EventListener.get());
        connectorServer.start();
    }

    public static void startConnectorServer(String password) {
        try {
            INSTANCE.tcpPort = NetworkUtil.findFreePort();
            INSTANCE.serverPassword = password;
            INSTANCE.connectorServer.bind(INSTANCE.tcpPort);
            System.out.println("Connector server up at port " + INSTANCE.tcpPort);
            ChatView.addChatMessage("Server up at port: " + INSTANCE.tcpPort);
        } catch (IOException e) {
            //TODO handle server start failed
            e.printStackTrace();
        }
    }

    public static Server getConnectorServer() {
        return INSTANCE.connectorServer;
    }

    static int getTcpPort() {
        return INSTANCE.tcpPort;
    }

    public static boolean passwordInvalid(String password) {
        if (null == INSTANCE.serverPassword || "".equals(INSTANCE.serverPassword)) {
            return false;
        }

        if (INSTANCE.serverPassword.equals(password)) {
            return false;
        }

        return true;
    }
}
