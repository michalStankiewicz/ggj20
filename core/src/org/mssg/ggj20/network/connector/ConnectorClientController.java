package org.mssg.ggj20.network.connector;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.KryoSerialization;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.network.common.EventListener;
import org.mssg.ggj20.network.connector.objects.ConnectionInitEvent;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

import java.io.IOException;

public class ConnectorClientController {

    private static final ConnectorClientController INSTANCE = new ConnectorClientController();
    private static final int TIMEOUT = CFG.getInt("multiplayer.timeout");
    private static final String LOCALHOST = "127.0.0.1";

    private Client connectorClient;

    private ConnectorClientController() {
        connectorClient = new Client(1024, 1024, new KryoSerialization());
        connectorClient.addListener(EventListener.get());
        connectorClient.start();
    }

    public static void joinLocal(String playerName, String password) {
        ConnectionInitEvent connectionInitEvent = new ConnectionInitEvent(playerName,
                password,
                LOCALHOST);
        join(LOCALHOST, ConnectorServerController.getTcpPort(), connectionInitEvent);
    }

    public static void join(String host, int tcpPort, ConnectionInitEvent connectionInitEvent) {
        GameClientController.getGameState().reset();
        try {
            INSTANCE.connectorClient.connect(TIMEOUT, host, tcpPort);
        } catch (IOException ex) {
            MenuStage.getInstance().showError("Can't connect");
            ex.printStackTrace();
        }

        INSTANCE.connectorClient.sendTCP(connectionInitEvent);
    }

    public static Client getConnectorClient() {
        return INSTANCE.connectorClient;
    }
}
