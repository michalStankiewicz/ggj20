package org.mssg.ggj20.network.connector.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.connector.ConnectorServerController;
import org.mssg.ggj20.network.connector.objects.ConnectionInitEvent;
import org.mssg.ggj20.network.connector.objects.InvalidPasswordEvent;
import org.mssg.ggj20.network.connector.objects.PlayerInvitationEvent;
import org.mssg.ggj20.network.game.GameServerController;

public class ConnectionInitHandler implements ClientEventHandler<ConnectionInitEvent> {

    @Override
    public void processMessage(Connection connection, ConnectionInitEvent connectionInitEvent) {
        if (ConnectorServerController.passwordInvalid(connectionInitEvent.getPassword())) {
            connection.sendTCP(new InvalidPasswordEvent());
            return;
        }

        /*
        TODO
        2. Check username
            If taken - generate new (with _1 or something)

        3. Send PlayerInvitationEvent:
            tcpPort, udpPort, new playername
         */
        PlayerInvitationEvent playerInvitationEvent = new PlayerInvitationEvent();

        playerInvitationEvent.setPlayerName(connectionInitEvent.getName());
        playerInvitationEvent.setHost(connectionInitEvent.getTargetHost());
        playerInvitationEvent.setTcpPort(GameServerController.getTcpPort());
        playerInvitationEvent.setUdpPort(GameServerController.getUdpPort());

        connection.sendTCP(playerInvitationEvent);


    }
}
