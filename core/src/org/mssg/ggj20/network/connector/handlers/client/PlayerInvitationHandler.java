package org.mssg.ggj20.network.connector.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.connector.ConnectorClientController;
import org.mssg.ggj20.network.connector.objects.PlayerInvitationEvent;
import org.mssg.ggj20.network.game.GameClientController;

public class PlayerInvitationHandler implements ServerEventHandler<PlayerInvitationEvent> {

    @Override
    public void processMessage(Connection connection, PlayerInvitationEvent playerInvitationEvent) {
        GameClientController.join(playerInvitationEvent);
        ConnectorClientController.getConnectorClient().close();
    }
}
