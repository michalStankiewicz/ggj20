package org.mssg.ggj20.network.connector.objects;

import org.mssg.ggj20.network.common.ClientEvent;

public class ConnectionInitEvent extends ClientEvent {

    String name;
    String password;
    private String targetHost;

    public ConnectionInitEvent() {
    }

    public ConnectionInitEvent(String name, String password, String targetHost) {
        this.name = name;
        this.password = password;
        this.targetHost = targetHost;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getTargetHost() {
        return targetHost;
    }

    public void setTargetHost(String targetHost) {
        this.targetHost = targetHost;
    }
}
