package org.mssg.ggj20.network.connector.objects;

import org.mssg.ggj20.network.common.ServerEvent;

public class PlayerInvitationEvent extends ServerEvent {

    private String host;
    private int tcpPort;
    private int udpPort;
    private String playerName;

    public PlayerInvitationEvent() {
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getTcpPort() {
        return tcpPort;
    }

    public void setTcpPort(int tcpPort) {
        this.tcpPort = tcpPort;
    }

    public int getUdpPort() {
        return udpPort;
    }

    public void setUdpPort(int udpPort) {
        this.udpPort = udpPort;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
