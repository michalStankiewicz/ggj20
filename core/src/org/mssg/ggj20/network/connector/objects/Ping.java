package org.mssg.ggj20.network.connector.objects;

import org.mssg.ggj20.network.common.ServerEvent;

public class Ping extends ServerEvent {
    private int id;
    private long tick;

    public Ping() {
    }

    public Ping(int id, long tick) {
        this.id = id;
        this.tick = tick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }
}
