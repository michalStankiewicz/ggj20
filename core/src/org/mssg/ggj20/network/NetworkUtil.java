package org.mssg.ggj20.network;

import org.mssg.ggj20.network.common.Event;
import org.mssg.ggj20.network.connector.ConnectorClientController;
import org.mssg.ggj20.network.connector.ConnectorServerController;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.GameServerController;

import java.io.IOException;
import java.net.ServerSocket;

public class NetworkUtil {

    public static int findFreePort() {
        int port = -1;
        try {
            ServerSocket socket = new ServerSocket(0);
            port = socket.getLocalPort();
            socket.close();
            return port;
        } catch (IOException ioe) {
            return port;
        }
    }

    static <T extends Event> void registerClass(Class<T> clazz) {
        ConnectorServerController.getConnectorServer().getKryo().register(clazz);
        ConnectorClientController.getConnectorClient().getKryo().register(clazz);
        GameServerController.getServer().getKryo().register(clazz);
        GameClientController.getClient().getKryo().register(clazz);
    }

    static void registerClasses(Class<?>... moarClazz) {
        for (int i = 0; i < moarClazz.length; i++) {
            ConnectorServerController.getConnectorServer().getKryo().register(moarClazz[i]);
            ConnectorClientController.getConnectorClient().getKryo().register(moarClazz[i]);
            GameServerController.getServer().getKryo().register(moarClazz[i]);
            GameClientController.getClient().getKryo().register(moarClazz[i]);
        }
    }
}
