package org.mssg.ggj20.network;

import org.mssg.ggj20.network.common.ClientEvent;
import org.mssg.ggj20.network.common.EventHandler;
import org.mssg.ggj20.network.common.ServerEvent;

import java.util.HashMap;
import java.util.Map;

public class MessageHandlers {

    private static final Map<Class<? extends ServerEvent>, EventHandler> serverEventHandlers = new HashMap<>();
    private static final Map<Class<? extends ClientEvent>, EventHandler> clientEventHandlers = new HashMap<>();

    private static EventHandler defaultHandler;

    public static EventHandler getFor(Class<?> clazz) {
        if (ServerEvent.class.isAssignableFrom(clazz)) {
            return serverEventHandlers.get(clazz);
        } else if (ClientEvent.class.isAssignableFrom(clazz)) {
            return clientEventHandlers.get(clazz);
        } else {
            return defaultHandler;
        }
    }

    static <T extends ClientEvent> void registerClientEvent(Class<T> clazz,
                                                            EventHandler<T> handler) {
        clientEventHandlers.put(clazz, handler);
        NetworkUtil.registerClass(clazz);
    }

    static <T extends ServerEvent> void registerServerEvent(Class<T> clazz,
                                                            EventHandler<T> handler) {
        serverEventHandlers.put(clazz, handler);
        NetworkUtil.registerClass(clazz);
    }

    static void setDefaultHandler(EventHandler handler) {
        defaultHandler = handler;
    }
}
