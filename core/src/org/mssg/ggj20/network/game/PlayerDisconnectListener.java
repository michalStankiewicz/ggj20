package org.mssg.ggj20.network.game;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import org.mssg.ggj20.game.Player;
import org.mssg.ggj20.network.game.events.ChatEvent;
import org.mssg.ggj20.network.game.events.PlayersListEvent;

class PlayerDisconnectListener extends Listener {
    private static final PlayerDisconnectListener THIS = new PlayerDisconnectListener();

    public static Listener get() {
        return THIS;
    }

    @Override
    public void disconnected(Connection connection) {
        super.disconnected(connection);
        ChatEvent chatEvent = new ChatEvent();

        Player player = GameServerController.getGameState()
                .getPlayers().get(connection.getID());

        if (player == null) {
            return;
        }

        String playerName = player.getName();
        chatEvent.setMessage("Player " + playerName + " disconnected");

        GameServerController.getServer()
                .sendToAllTCP(chatEvent);

        GameServerController.getGameState().removePlayer(connection.getID());

        PlayersListEvent playersList = new PlayersListEvent();
        playersList.setPlayers(GameServerController.getGameState().getPlayers());
        GameServerController.getServer()
                .sendToAllTCP(playersList);
    }
}
