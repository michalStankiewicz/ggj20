package org.mssg.ggj20.network.game;

import org.mssg.ggj20.game.input.InputMonitor;
import org.mssg.ggj20.network.game.events.PlayerInputEvent;

public class ClientTickCounter extends TickCounter {

    private static final int CONTROLS_BUFFER_SIZE = 8;
    private byte[] inputHistory = new byte[CONTROLS_BUFFER_SIZE];

    @Override
    protected void doExtraStuff(long tick) {
        GameClientController.getGameState().updateState();
        byte currentInput = InputMonitor.collect();

        System.arraycopy(inputHistory, 1, inputHistory, 0, CONTROLS_BUFFER_SIZE - 1);
        inputHistory[CONTROLS_BUFFER_SIZE - 1] = currentInput;

        PlayerInputEvent playerInputEvent = new PlayerInputEvent();
        playerInputEvent.setControls(inputHistory);
        playerInputEvent.setFirstTick(tick - CONTROLS_BUFFER_SIZE);

        GameClientController.getClient().sendUDP(playerInputEvent);
    }
}
