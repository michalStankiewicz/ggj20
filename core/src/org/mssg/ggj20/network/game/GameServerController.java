package org.mssg.ggj20.network.game;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.KryoSerialization;
import com.esotericsoftware.kryonet.Server;

import org.mssg.ggj20.game.GameState;
import org.mssg.ggj20.network.NetworkUtil;
import org.mssg.ggj20.network.common.EventListener;
import org.mssg.ggj20.network.connector.objects.Ping;
import org.mssg.ggj20.network.game.server.PingPongArray;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

public class GameServerController {

    private static final GameServerController INSTANCE = new GameServerController();
    private static MetaGameState metaGameState = new MetaGameState();
    private GameState gameState = new GameState();
    private boolean iAmHost;
    private TickCounter tickCounter = new ServerTickCounter();

    private Server server;
    private int tcpPort;

    private int udpPort;

    public GameServerController() {
        iAmHost = false;
        server = new Server(1024, 1024, new KryoSerialization());

        server.addListener(EventListener.get());
        server.addListener(PlayerDisconnectListener.get());
        server.start();
    }

    public static void startGameServer(String serverPassword) {
        INSTANCE.gameState.reset();
        try {
            INSTANCE.tcpPort = NetworkUtil.findFreePort();
            INSTANCE.udpPort = NetworkUtil.findFreePort();
            INSTANCE.server.bind(INSTANCE.tcpPort, INSTANCE.udpPort);
            System.out.println("Game server is up. TCP port: " + INSTANCE.tcpPort + ", UDP port: " + INSTANCE.udpPort);
            INSTANCE.iAmHost = true;
        } catch (IOException e) {
            //TODO handle server start failed
            e.printStackTrace();
        }
    }

    public static int getTcpPort() {
        return INSTANCE.tcpPort;
    }

    public static int getUdpPort() {
        return INSTANCE.udpPort;
    }

    public static GameState getGameState() {
        return INSTANCE.gameState;
    }

    public static MetaGameState getMetaGameState() {
        return metaGameState;
    }

    public static void setMetaGameState(MetaGameState metaGameState) {
        GameServerController.metaGameState = metaGameState;
    }

    public static Server getServer() {
        return INSTANCE.server;
    }

    public static boolean amIHost() {
        return INSTANCE.iAmHost;
    }

//    public GameState getGameState() {
//        return gameState;
//    }

    public static void launchTickSync(Connection connection) {
        INSTANCE.runTickSynchro(connection);
    }

    public static TickCounter getTickCounter() {
        return INSTANCE.tickCounter;
    }

    public String checkName(String playerDataName) {
        return null;
    }

    private void runTickSynchro(Connection connection) {
        int connectionId = connection.getID();
        final PingPongArray pingPong = GameServerController.getGameState()
                .getPingPong().get(connectionId);

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            int pingsLeft = 19;

            @Override
            public void run() {
                Ping ping = new Ping(pingsLeft, GameServerController.getTickCounter().getTick());
                connection.sendUDP(ping);
                pingPong.clear();
                pingPong.addPing(ping);

                if (--pingsLeft < 0) {
                    timer.cancel();
                    timer.purge();

                    Timer timer2 = new Timer();
                    timer2.schedule(new TimerTask() {
                        @Override
                        public void run() {
                            GameServerController.getGameState().syncTick();
                        }
                    }, 2000);
                }
            }
        }, 0, 50);
    }
}
