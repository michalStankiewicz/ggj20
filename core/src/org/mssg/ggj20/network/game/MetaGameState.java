package org.mssg.ggj20.network.game;

import org.mssg.ggj20.game.Player;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class MetaGameState {
    Map<Integer, Data> playerData = new ConcurrentHashMap<>();

    public void setPlayers(Map<Integer, Player> players) {
        playerData.clear();
        Integer[] keySet = players.keySet().toArray(new Integer[0]);
        for (int i = 0; i < keySet.length; i++) {
            playerData.put(keySet[i], new Data());
        }
    }

    public void playeLoaded(int id) {
        playerData.get(id).setGameLoaded(true);
    }

    public boolean allLoaded() {
        Data[] datas = playerData.values().toArray(new Data[0]);
        for (int i = 0; i < datas.length; i++) {
            if (!datas[i].isGameLoaded()) {
                return false;
            }
        }
        return true;
    }

    private class Data {
        boolean gameLoaded = false;

        public boolean isGameLoaded() {
            return gameLoaded;
        }

        public void setGameLoaded(boolean gameLoaded) {
            this.gameLoaded = gameLoaded;
        }
    }
}
