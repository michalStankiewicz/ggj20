package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.PlayerCharacterChangedEvent;
import org.mssg.ggj20.network.game.events.PlayersListEvent;

public class PlayerCharacterChangedEventHandler implements ClientEventHandler<PlayerCharacterChangedEvent> {
    @Override
    public void processMessage(Connection connection, PlayerCharacterChangedEvent playerCharacterChangedEvent) {
        GameServerController.getGameState().getPlayers().get(playerCharacterChangedEvent.getConnectionId())
                .setCharacter(playerCharacterChangedEvent.getCharacter());

        PlayersListEvent playersList = new PlayersListEvent();
        playersList.setPlayers(GameServerController.getGameState().getPlayers());

        GameServerController.getServer().sendToAllTCP(playersList);
    }
}
