package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.Pong;
import org.mssg.ggj20.network.game.server.PingPongArray;

public class PongHandler implements ClientEventHandler<Pong> {
    @Override
    public void processMessage(Connection connection, Pong pong) {
        pong.setTick(GameServerController.getTickCounter().getTick());
        PingPongArray pingPong = GameServerController.getGameState()
                .getPingPong().get(connection.getID());

        pingPong.addPong(pong);
    }
}
