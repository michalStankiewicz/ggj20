package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.commons.ui.ChatView;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.TickReceivedEvent;
import org.mssg.ggj20.network.game.events.TickUpdateEvent;

public class TickUpdateHandler implements ServerEventHandler<TickUpdateEvent> {
    @Override
    public void processMessage(Connection connection, TickUpdateEvent tickUpdateEvent) {
        GameClientController.getTickCounter().setTick(tickUpdateEvent.getTick());
        ChatView.addChatMessage("Tick updated: " + tickUpdateEvent.getTick());
        TickReceivedEvent tickReceivedEvent = new TickReceivedEvent();
        GameClientController.getClient().sendTCP(tickReceivedEvent);
    }
}
