package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.assets.SND;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.ItGuyNeedsHelpEvent;

import static org.mssg.ggj20.assets.SND.NEW_PROBLEM;

public class ItGuyNeedsHelpEventHandler implements ServerEventHandler<ItGuyNeedsHelpEvent> {
    @Override
    public void processMessage(Connection connection, ItGuyNeedsHelpEvent itGuyNeedsHelpEvent) {
        SND.play(NEW_PROBLEM);
        GameClientController.getGameState().markItGuysNeedHelp(
                itGuyNeedsHelpEvent.getItGuyId()
        );
    }
}
