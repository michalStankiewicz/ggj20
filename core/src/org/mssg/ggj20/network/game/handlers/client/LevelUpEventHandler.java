package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.events.LevelUpEvent;
import org.mssg.ggj20.screens.game.HUD;

public class LevelUpEventHandler implements ServerEventHandler<LevelUpEvent> {
    @Override
    public void processMessage(Connection connection, LevelUpEvent levelUpEvent) {
        HUD.get().updateLevel(levelUpEvent.getLevel());
    }
}
