package org.mssg.ggj20.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.PlayersListEvent;
import org.mssg.ggj20.screens.lobby.PlayerSummaryView;

public class PlayersListHandler implements ServerEventHandler<PlayersListEvent> {

    @Override
    public void processMessage(Connection connection, PlayersListEvent playersListEvent) {
        Gdx.app.postRunnable(() -> GameClientController.getGameState()
                .updatePlayers(playersListEvent.getPlayers()));
        PlayerSummaryView.get().updatePlayerStatus(playersListEvent.getPlayers());
    }
}
