package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.game.Player;
import org.mssg.ggj20.network.common.EventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.ChatEvent;
import org.mssg.ggj20.network.game.events.PlayerJoinConfirmEvent;
import org.mssg.ggj20.network.game.events.PlayerJoinEvent;
import org.mssg.ggj20.network.game.events.PlayersListEvent;
import org.mssg.ggj20.network.game.events.ServerFullEvent;
import org.mssg.ggj20.screens.lobby.PlayerCharacter;

public class PlayerJoinHandler implements EventHandler<PlayerJoinEvent> {

    private static final int PLAYERS_NEEDED = CFG.getInt("multiplayer.maxPlayers");

    @Override
    public void processMessage(Connection connection, PlayerJoinEvent playerJoinEvent) {
        if (GameServerController.getGameState().getPlayers().size() >= PLAYERS_NEEDED) {
            ServerFullEvent serverFullEvent = new ServerFullEvent();
            connection.sendTCP(serverFullEvent);
            return;
        }

        if (GameServerController.getGameState().isGameOn()) {
//            CantJoinEvent cantJoinEvent = new CantJoinEvent();
//            connection.sendTCP(cantJoinEvent);
            return;
        }

        //add player
        Player player = new Player();
        player.setName(playerJoinEvent.getPlayerName());
        player.setId(connection.getID());
        player.setCharacter(PlayerCharacter.values()[
                GameServerController.getGameState().getPlayers().size()]);
        GameServerController.getGameState().addPlayer(connection.getID(), player);

        //prepare list of all players
        PlayersListEvent playersList = new PlayersListEvent();
        playersList.setPlayers(GameServerController.getGameState().getPlayers());

        //confirm player joined
        PlayerJoinConfirmEvent playerJoinConfirmEvent = new PlayerJoinConfirmEvent();
        playerJoinConfirmEvent.setConnectionId(connection.getID());
        connection.sendTCP(playerJoinConfirmEvent);

        ChatEvent chatEvent = new ChatEvent("Player " + playerJoinEvent.getPlayerName() + " joined");
        GameServerController.getServer().sendToAllTCP(chatEvent);


        //send it
        GameServerController.getServer().sendToAllTCP(playersList);
    }
}
