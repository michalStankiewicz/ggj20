package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.events.BoardMoodUpdateEvent;
import org.mssg.ggj20.screens.game.HUD;

public class BoardMoodUpdateEventHandler implements ServerEventHandler<BoardMoodUpdateEvent> {
    @Override
    public void processMessage(Connection connection, BoardMoodUpdateEvent boardMoodUpdateEvent) {
        HUD.get().updateMood(boardMoodUpdateEvent.getMood());
    }
}
