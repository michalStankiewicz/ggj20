package org.mssg.ggj20.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.PlayerJoinConfirmEvent;

public class PlayerJoinConfirmHandler implements ServerEventHandler<PlayerJoinConfirmEvent> {
    @Override
    public void processMessage(Connection connection, PlayerJoinConfirmEvent playerJoinConfirmEvent) {
        GameClientController.getGameState().setMyId(playerJoinConfirmEvent.getConnectionId());
        Gdx.app.postRunnable(GGJ20.INSTANCE::goToLobby);
    }
}
