package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.events.ServerFullEvent;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

public class ServerFullHandler implements ServerEventHandler<ServerFullEvent> {
    @Override
    public void processMessage(Connection connection, ServerFullEvent serverFullEvent) {
        MenuStage.getInstance().showError("Server full");
    }
}
