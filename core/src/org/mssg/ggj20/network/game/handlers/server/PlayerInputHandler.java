package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.PlayerInputEvent;

public class PlayerInputHandler implements ClientEventHandler<PlayerInputEvent> {
    @Override
    public void processMessage(Connection connection, PlayerInputEvent playerInputEvent) {
        GameServerController.getGameState().bufferInput(
                connection.getID(), playerInputEvent);
    }
}
