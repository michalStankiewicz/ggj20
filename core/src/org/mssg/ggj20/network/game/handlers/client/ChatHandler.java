package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.commons.ui.ChatView;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.events.ChatEvent;

public class ChatHandler implements ServerEventHandler<ChatEvent> {
    @Override
    public void processMessage(Connection connection, ChatEvent chatEvent) {
        //TODO clean up events/messages
        ChatView.addChatMessage(chatEvent.getMessage());
    }
}
