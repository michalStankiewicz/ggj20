package org.mssg.ggj20.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.events.GameOverEvent;

public class GameOverHandler implements ServerEventHandler<GameOverEvent> {
    @Override
    public void processMessage(Connection connection, GameOverEvent gameOverEvent) {
        Gdx.app.postRunnable(() -> GGJ20.INSTANCE.goToGameOverScreen(gameOverEvent.getScore()));
    }
}
