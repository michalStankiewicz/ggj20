package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.PlayerSolvedProblem;
import org.mssg.ggj20.screens.game.HUD;

public class PlayerSolvedProblemHandler implements ServerEventHandler<PlayerSolvedProblem> {
    @Override
    public void processMessage(Connection connection, PlayerSolvedProblem playerSolvedProblem) {
        GameClientController.getGameState().problemSolved(playerSolvedProblem);
        HUD.get().updateScore(playerSolvedProblem.getScore());
    }
}
