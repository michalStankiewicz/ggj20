package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.LoadedGameEvent;

public class LoadedGameEventHandler implements ClientEventHandler<LoadedGameEvent> {
    @Override
    public void processMessage(Connection connection, LoadedGameEvent loadedGameEvent) {
        GameServerController.getMetaGameState().playeLoaded(connection.getID());
        if (GameServerController.getMetaGameState().allLoaded()) {
            GameServerController.launchTickSync(connection);
        }
    }
}
