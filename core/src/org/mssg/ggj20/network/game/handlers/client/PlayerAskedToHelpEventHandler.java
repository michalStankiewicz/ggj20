package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.PlayerAskedToHelpEvent;

public class PlayerAskedToHelpEventHandler implements ServerEventHandler<PlayerAskedToHelpEvent> {
    @Override
    public void processMessage(Connection connection, PlayerAskedToHelpEvent playerAskedToHelpEvent) {
        GameClientController.getGameState().itGuyNeedsSpecificHelp(
                playerAskedToHelpEvent.getAskedToHelp(),
                playerAskedToHelpEvent.getHelpDetails());
    }
}
