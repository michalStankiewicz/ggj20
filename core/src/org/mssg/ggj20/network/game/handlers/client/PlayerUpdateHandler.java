package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.PlayerUpdateEvent;

public class PlayerUpdateHandler implements ServerEventHandler<PlayerUpdateEvent> {
    @Override
    public void processMessage(Connection connection, PlayerUpdateEvent playerUpdateEvent) {
        GameClientController.getGameState().bufferPlayerUpdate(playerUpdateEvent);
    }
}
