package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.ChatEvent;
import org.mssg.ggj20.network.game.events.PlayerSendMessageEvent;

public class PlayerSendMessageHandler implements ClientEventHandler<PlayerSendMessageEvent> {
    @Override
    public void processMessage(Connection connection, PlayerSendMessageEvent playerSendMessageEvent) {
        ChatEvent chatEvent = new ChatEvent(playerSendMessageEvent.getPlayer() + ": " + playerSendMessageEvent.getMessage());
        GameServerController.getServer().sendToAllTCP(chatEvent);
    }
}
