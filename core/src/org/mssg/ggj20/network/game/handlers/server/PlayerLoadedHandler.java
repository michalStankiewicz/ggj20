package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.PlayerLoadedEvent;

public class PlayerLoadedHandler implements ClientEventHandler<PlayerLoadedEvent> {
    @Override
    public void processMessage(Connection connection, PlayerLoadedEvent playerLoadedEvent) {
        GameServerController.launchTickSync(connection);
    }
}
