package org.mssg.ggj20.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.LaunchGameEvent;

public class LaunchGameEventHandler implements ServerEventHandler<LaunchGameEvent> {
    @Override
    public void processMessage(Connection connection, LaunchGameEvent startGameEvent) {

        GameClientController.getTickCounter().start();
        Gdx.app.postRunnable(() -> GGJ20.INSTANCE.goToGameScreen());
    }
}
