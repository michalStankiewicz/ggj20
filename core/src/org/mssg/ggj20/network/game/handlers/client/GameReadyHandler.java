package org.mssg.ggj20.network.game.handlers.client;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.game.events.GameReadyEvent;

public class GameReadyHandler implements ServerEventHandler<GameReadyEvent> {
    @Override
    public void processMessage(Connection connection, GameReadyEvent gameReadyEvent) {
        Gdx.app.postRunnable(GGJ20.INSTANCE::goToGameScreen);
    }
}
