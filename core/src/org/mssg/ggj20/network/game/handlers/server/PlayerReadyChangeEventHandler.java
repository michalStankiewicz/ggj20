package org.mssg.ggj20.network.game.handlers.server;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.game.Player;
import org.mssg.ggj20.network.common.ClientEventHandler;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.ChatEvent;
import org.mssg.ggj20.network.game.events.LaunchGameEvent;
import org.mssg.ggj20.network.game.events.PlayerReadyChangeEvent;
import org.mssg.ggj20.network.game.events.PlayersListEvent;

public class PlayerReadyChangeEventHandler implements ClientEventHandler<PlayerReadyChangeEvent> {
    private boolean skip = CFG.getBoolean("magicDonkey");

    @Override
    public void processMessage(Connection connection, PlayerReadyChangeEvent playerReadyChangeEvent) {
        GameServerController.getGameState().getPlayers().get(playerReadyChangeEvent.getConnectionId())
                .setReady(playerReadyChangeEvent.getReady());

        PlayersListEvent playersList = new PlayersListEvent();
        playersList.setPlayers(GameServerController.getGameState().getPlayers());

        GameServerController.getServer().sendToAllTCP(playersList);

        if (canStartGame()) {
            GameServerController.getServer().sendToAllTCP(new ChatEvent("Starting game..."));
            GameServerController.getMetaGameState().setPlayers(
                    GameServerController.getGameState().getPlayers());


            GameServerController.getGameState().preparePhysics();
            GameServerController.getGameState().setGameOn(true);
            GameServerController.getTickCounter().start();
            GameServerController.getServer().sendToAllTCP(new LaunchGameEvent());
        }
    }

    private boolean canStartGame() {
        return allPlayersHaveDifferentCharacter()
                && allPlayersReady();
    }

    private boolean allPlayersReady() {
        Player[] players = GameServerController.getGameState().getPlayers().values().toArray(new Player[0]);
        for (int i = 0; i < players.length; i++) {
            if (!players[i].isReady()) {
                return false;
            }
        }
        return true;
    }

    private boolean allPlayersHaveDifferentCharacter() {
        if(skip){
            return true;
        }
        Player[] players = GameServerController.getGameState().getPlayers().values().toArray(new Player[0]);

        boolean donutRaptorTaken = false;
        boolean octoPieTaken = false;
        boolean cookieJarTaken = false;

        for(int i = 0; i < players.length; i++){
            switch (players[i].getCharacter()) {
                case OCTO_PIE:
                    octoPieTaken = true;
                    break;
                case COOKIE_JAR:
                    cookieJarTaken = true;
                    break;
                case DONUT_RAPTOR:
                    donutRaptorTaken = true;
                    break;
            }
        }

        if(!donutRaptorTaken){
            GameServerController.getServer().sendToAllTCP(new ChatEvent("You need DONUT RAPTOR!"));
        }

        if(!octoPieTaken){
            GameServerController.getServer().sendToAllTCP(new ChatEvent("You need OCTO PIE!"));
        }

        if(!cookieJarTaken){
            GameServerController.getServer().sendToAllTCP(new ChatEvent("You need COOKIE JAR!"));
        }

        return donutRaptorTaken && octoPieTaken && cookieJarTaken;
    }
}
