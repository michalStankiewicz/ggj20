package org.mssg.ggj20.network.game.handlers.client;

import com.esotericsoftware.kryonet.Connection;

import org.mssg.ggj20.network.common.ServerEventHandler;
import org.mssg.ggj20.network.connector.objects.Ping;
import org.mssg.ggj20.network.game.events.Pong;

public class PingHandler implements ServerEventHandler<Ping> {
    @Override
    public void processMessage(Connection connection, Ping ping) {
        connection.sendUDP(new Pong(ping.getId(), ping.getTick()));
    }
}
