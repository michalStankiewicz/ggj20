package org.mssg.ggj20.network.game.server;

import org.mssg.ggj20.network.connector.objects.Ping;
import org.mssg.ggj20.network.game.events.Pong;

public class PingPongArray {
    public long[] pings;
    public long[] pongs;
    public long delay;

    public PingPongArray() {
        this.pings = new long[20];
        this.pongs = new long[20];
    }

    public void clear() {
        for (int i = 0; i < 20; i++) {
            pings[i] = -1;
            pongs[i] = -1;
        }
    }

    public void addPing(Ping ping) {
        pings[ping.getId()] = ping.getTick();
    }

    public void addPong(Pong pong) {
        pongs[pong.getId()] = pong.getTick();
    }

    public long calculateDelay() {
        long total = 0;
        int packets = 0;

        for (int i = 0; i < 20; i++) {
            if (pongs[i] != -1) {
                total += (pongs[i] - pings[i]);
                packets++;
            }
        }

        delay = (long) (total * 0.5 / packets);
        return delay;
    }
}
