package org.mssg.ggj20.network.game;

import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.KryoSerialization;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.game.GameState;
import org.mssg.ggj20.network.common.EventListener;
import org.mssg.ggj20.network.connector.objects.PlayerInvitationEvent;
import org.mssg.ggj20.network.game.events.PlayerJoinEvent;

import java.io.IOException;

public class GameClientController {
    private static final GameClientController INSTANCE = new GameClientController();

    private static final int TIMEOUT = CFG.getInt("multiplayer.timeout");
    private GameState gameState;
    private TickCounter tickCounter = new ClientTickCounter();

    private Client client;

    public GameClientController() {
        client = new Client(1024, 1024, new KryoSerialization());
        client.addListener(EventListener.get());
        client.addListener(ServerClosedListener.get());
        gameState = new GameState();
        client.start();
    }

    public static void join(PlayerInvitationEvent playerInvitationEvent) {
        try {
            INSTANCE.client.connect(TIMEOUT, playerInvitationEvent.getHost(),
                    playerInvitationEvent.getTcpPort(),
                    playerInvitationEvent.getUdpPort());
            PlayerJoinEvent playerJoinEvent = new PlayerJoinEvent();
            playerJoinEvent.setPlayerName(playerInvitationEvent.getPlayerName());
            INSTANCE.client.sendTCP(playerJoinEvent);
            getGameState().setMe(playerInvitationEvent.getPlayerName());

        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public static Client getClient() {
        return INSTANCE.client;
    }

    public static GameState getGameState() {
        return INSTANCE.gameState;
    }

    public static TickCounter getTickCounter() {
        return INSTANCE.tickCounter;
    }
}
