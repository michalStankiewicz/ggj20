package org.mssg.ggj20.network.game;

import com.badlogic.gdx.Gdx;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

class ServerClosedListener extends Listener {
    private static final ServerClosedListener THIS = new ServerClosedListener();

    public static Listener get() {
        return THIS;
    }

    @Override
    public void disconnected(Connection connection) {
        Gdx.app.postRunnable(() -> {
            GGJ20.INSTANCE.goToMainMenuScreen();
            if (!GameServerController.amIHost()) {
                MenuStage.getInstance().showError("Disconnected");
            }
        });
    }
}
