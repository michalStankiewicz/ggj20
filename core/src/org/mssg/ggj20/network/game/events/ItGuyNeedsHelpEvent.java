package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ServerEvent;

public class ItGuyNeedsHelpEvent extends ServerEvent {

    private int itGuyId;

    public ItGuyNeedsHelpEvent() {
    }

    public ItGuyNeedsHelpEvent(int itGuyId) {

        this.itGuyId = itGuyId;
    }

    public int getItGuyId() {
        return itGuyId;
    }

    public void setItGuyId(int itGuyId) {
        this.itGuyId = itGuyId;
    }
}
