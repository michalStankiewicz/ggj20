package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.game.Player;
import org.mssg.ggj20.network.common.ServerEvent;

import java.util.Map;

public class PlayersListEvent extends ServerEvent {
    private Map<Integer, Player> players;

    public Map<Integer, Player> getPlayers() {
        return players;
    }

    public void setPlayers(Map<Integer, Player> players) {
        this.players = players;
    }
}
