package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ServerEvent;

public class TickUpdateEvent extends ServerEvent {
    private long tick;

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }
}
