package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ClientEvent;

public class PlayerReadyChangeEvent extends ClientEvent {
    private int connectionId;
    private boolean ready;

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }

    public boolean getReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }
}
