package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ServerEvent;

public class GameOverEvent extends ServerEvent {
    private int score;

    public GameOverEvent() {
    }

    public GameOverEvent(int score) {
        this.score = score;
    }

    public int getScore() {
        return score;
    }
}
