package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ServerEvent;

public class PlayerJoinConfirmEvent extends ServerEvent {
    private int connectionId;

    public PlayerJoinConfirmEvent() {
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }
}
