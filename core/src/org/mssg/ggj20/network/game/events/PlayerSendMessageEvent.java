package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ClientEvent;

public class PlayerSendMessageEvent extends ClientEvent {
    private String message;
    private String player;

    public PlayerSendMessageEvent() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPlayer() {
        return player;
    }

    public void setPlayer(String player) {
        this.player = player;
    }
}
