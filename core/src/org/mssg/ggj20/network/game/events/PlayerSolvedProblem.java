package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ServerEvent;

public class PlayerSolvedProblem extends ServerEvent {
    private int playerId;
    private Integer happyItGuyId;
    private int score;

    public PlayerSolvedProblem() {
    }

    public PlayerSolvedProblem(int playerId, Integer happyItGuyId, int score) {
        this.playerId = playerId;
        this.happyItGuyId = happyItGuyId;
        this.score = score;
    }

    public Integer getHappyItGuyId() {
        return happyItGuyId;
    }

    public int getScore() {
        return score;
    }

    public int getPlayerId() {
        return playerId;
    }
}
