package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ClientEvent;

public class PlayerInputEvent extends ClientEvent {

    private byte[] controls;
    private long firstTick;

    public byte[] getControls() {
        return controls;
    }

    public void setControls(byte[] controls) {
        this.controls = controls;
    }

    public long getFirstTick() {
        return firstTick;
    }

    public void setFirstTick(long firstTick) {
        this.firstTick = firstTick;
    }

    @Override
    public String toString() {
        return "PlayerInputEvent{" +
                "controls=" + controls +
                ", firstTick=" + firstTick +
                '}';
    }
}
