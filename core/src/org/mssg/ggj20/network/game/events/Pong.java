package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ClientEvent;

public class Pong extends ClientEvent {
    private int id;
    private long tick;

    public Pong() {

    }

    public Pong(int id, long tick) {
        this.id = id;
        this.tick = tick;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTick() {
        return tick;
    }

    public void setTick(long tick) {
        this.tick = tick;
    }
}
