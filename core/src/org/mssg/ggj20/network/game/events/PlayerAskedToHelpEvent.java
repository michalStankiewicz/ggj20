package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.game.HelpDetails;
import org.mssg.ggj20.network.common.ServerEvent;

public class PlayerAskedToHelpEvent extends ServerEvent {
    private Integer askedToHelp;
    private HelpDetails helpDetails;

    public PlayerAskedToHelpEvent() {
    }

    public PlayerAskedToHelpEvent(Integer askedToHelp, HelpDetails helpDetails) {
        this.askedToHelp = askedToHelp;
        this.helpDetails = helpDetails;
    }

    public Integer getAskedToHelp() {
        return askedToHelp;
    }

    public HelpDetails getHelpDetails() {
        return helpDetails;
    }
}
