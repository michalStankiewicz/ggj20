package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ServerEvent;

public class LevelUpEvent extends ServerEvent {
    private int level;

    public LevelUpEvent() {
    }

    public LevelUpEvent(int level) {
        this.level = level;
    }

    public int getLevel() {
        return level;
    }
}
