package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.game.BoardMood;
import org.mssg.ggj20.network.common.ServerEvent;

public class BoardMoodUpdateEvent extends ServerEvent {
    private BoardMood mood;

    public BoardMoodUpdateEvent() {
    }

    public BoardMoodUpdateEvent(BoardMood mood) {

        this.mood = mood;
    }

    public BoardMood getMood() {
        return mood;
    }
}
