package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ClientEvent;
import org.mssg.ggj20.screens.lobby.PlayerCharacter;

public class PlayerCharacterChangedEvent extends ClientEvent {
    private PlayerCharacter character;
    private int connectionId;

    public PlayerCharacter getCharacter() {
        return character;
    }

    public void setCharacter(PlayerCharacter character) {
        this.character = character;
    }

    public int getConnectionId() {
        return connectionId;
    }

    public void setConnectionId(int connectionId) {
        this.connectionId = connectionId;
    }
}
