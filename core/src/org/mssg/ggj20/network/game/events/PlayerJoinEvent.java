package org.mssg.ggj20.network.game.events;

import org.mssg.ggj20.network.common.ClientEvent;

public class PlayerJoinEvent extends ClientEvent {
    private String playerName;

    public PlayerJoinEvent() {
    }

    public PlayerJoinEvent(String playerName) {
        this.playerName = playerName;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }
}
