package org.mssg.ggj20.network.game;

import org.mssg.ggj20.assets.CFG;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class TickCounter {
    private static final int TICK_MILLIS = CFG.getInt("multiplayer.tickMillis");
    private static final ScheduledExecutorService tickCounterScheduler = Executors.newScheduledThreadPool(1);
    private AtomicLong tick;
    private ScheduledFuture<?> tickCounter = null;

    TickCounter() {
        tick = new AtomicLong(0L);
    }

    public void start() {
        if (tickCounter != null) {
            tickCounter.cancel(false);
        }

        tickCounter = tickCounterScheduler.scheduleAtFixedRate(
                this::run,
                0,
                TICK_MILLIS,
                TimeUnit.MILLISECONDS
        );
    }

    private void run() {
        doExtraStuff(tick.incrementAndGet());
    }

    protected void doExtraStuff(long l) {

    }

    public long getTick() {
        return tick.get();
    }

    public void setTick(long tick) {
        this.tick.set(tick);
    }

    public void stop() {
        if(tickCounter == null){
            return;
        }
        tickCounter.cancel(true);
    }
}
