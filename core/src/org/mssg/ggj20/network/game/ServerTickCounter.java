package org.mssg.ggj20.network.game;

public class ServerTickCounter extends TickCounter {

    @Override
    protected void doExtraStuff(long tick) {
        try {
            GameServerController.getGameState().simulateTicks(tick);
        } catch (Throwable e) {
            e.printStackTrace();
        }

    }
}
