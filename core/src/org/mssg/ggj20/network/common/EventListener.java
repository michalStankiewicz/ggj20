package org.mssg.ggj20.network.common;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import org.mssg.ggj20.network.MessageHandlers;

public class EventListener extends Listener {
    private static final EventListener THIS = new EventListener();

    private EventListener() {
    }

    public static EventListener get() {
        return THIS;
    }

    @Override
    public void received(Connection connection, Object object) {
//        try {
        MessageHandlers.getFor(object.getClass())
                .handleMessage(connection, object);
//        }
//        catch (Exception e) {
//            int k = 2;
//        }
    }
}
