package org.mssg.ggj20.network.common;

import com.esotericsoftware.kryonet.Connection;

public interface EventHandler<T> {

    default void handleMessage(Connection connection, Object object) {
        T t = (T) object;
        processMessage(connection, t);
    }

    void processMessage(Connection connection, T t);
}
