package org.mssg.ggj20.network.common;

import com.esotericsoftware.kryonet.Connection;

public class NopHandler implements EventHandler<Object> {

    @Override
    public void processMessage(Connection connection, Object o) {
        //nop
    }
}
