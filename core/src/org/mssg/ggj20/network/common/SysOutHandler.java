package org.mssg.ggj20.network.common;

import com.esotericsoftware.kryonet.Connection;

public class SysOutHandler implements EventHandler<Object> {

    @Override
    public void processMessage(Connection connection, Object o) {
        System.out.println(o);
    }
}
