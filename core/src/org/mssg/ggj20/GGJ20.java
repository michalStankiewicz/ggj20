package org.mssg.ggj20;

import com.badlogic.gdx.Game;

import org.mssg.ggj20.assets.AssetsLoader;
import org.mssg.ggj20.assets.SND;
import org.mssg.ggj20.network.NetworkController;
import org.mssg.ggj20.screens.GameLoadingScreen;
import org.mssg.ggj20.screens.game.GameScreen;
import org.mssg.ggj20.screens.gameover.GameOverScreen;
import org.mssg.ggj20.screens.gameover.GameOverView;
import org.mssg.ggj20.screens.lobby.LobbyScreen;
import org.mssg.ggj20.screens.mainmenu.MenuScreen;

public class GGJ20 extends Game {

    public static GGJ20 INSTANCE;
    private GameLoadingScreen gameLoadingScreen;
    private MenuScreen mainMenuScreen;
    private LobbyScreen lobbyScreen;
    private GameScreen gameScreen;
    private GameOverScreen gameOverScreen;

    public void goToLobby() {
        setScreen(lobbyScreen);
    }

    public void goToGameScreen() {
        setScreen(gameScreen);
        SND.MUSIC.play();
    }

    @Override
    public void create() {
        INSTANCE = this;
        AssetsLoader.loadAllAssets();
        gameLoadingScreen = new GameLoadingScreen();
        NetworkController.prepareNetworkStuff();
        setScreen(gameLoadingScreen);
    }

    @Override
    public void dispose() {

    }

    public void finishLoading() {
        AssetsLoader.initializeAssets();
        createAllOtherScreens();
        setScreen(mainMenuScreen);
    }

    private void createAllOtherScreens() {
        mainMenuScreen = new MenuScreen();
        lobbyScreen = new LobbyScreen();
        gameScreen = new GameScreen();
        gameOverScreen = new GameOverScreen();
    }

    public void goToMainMenuScreen() {
        setScreen(mainMenuScreen);
    }

    public void goToGameOverScreen(int score) {
        GameOverView.INSTANCE.setScore(score);
        setScreen(gameOverScreen);
    }


}
