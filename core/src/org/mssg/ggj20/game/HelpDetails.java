package org.mssg.ggj20.game;

public enum HelpDetails {
    UNKNOWN,
    BUG,
    DONUT,
    COMPUTER;

    public static HelpDetails random() {
        return HelpDetails.values()[(int) (Math.random() * 3 + 1)];
    }
}
