package org.mssg.ggj20.game;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.screens.game.entities.CollisionType;

import java.util.List;

public class MapLoader {

    private static final BodyDef WALL_BODY_DEF = new BodyDef();
    private static final FixtureDef WALL_FIXTURE_DEF = new FixtureDef();


    private static final int PIX_PER_TILE = CFG.getInt("tileset.pixPerTile");

    public void loadMap(TiledMap map, World world, List<Body> trackedBodies) {
        GameClientController.getGameState().clearPlayerEntities();
        addWalls(map, world, "WALLS_PHYSICS", trackedBodies);
    }

    private void addWalls(TiledMap map, World world, String layer, List<Body> trackedBodies) {
        MapLayer wallsLayer = map.getLayers().get(layer);

        for (MapObject wall : wallsLayer.getObjects()) {
            float x = (float) wall.getProperties().get("x") / (2 * PIX_PER_TILE);
            float y = (float) wall.getProperties().get("y") / (2 * PIX_PER_TILE);
            float width = (float) wall.getProperties().get("width") / (2 * PIX_PER_TILE);
            float height = (float) wall.getProperties().get("height") / (2 * PIX_PER_TILE);

            WALL_BODY_DEF.position.x = 2 * x + width;
            WALL_BODY_DEF.position.y = 2 * y + height;
            WALL_BODY_DEF.type = BodyDef.BodyType.StaticBody;

            PolygonShape shape = new PolygonShape();
            shape.setAsBox(width, height);
            WALL_FIXTURE_DEF.shape = shape;

            WALL_FIXTURE_DEF.filter.categoryBits = CollisionType.WALL;
            WALL_FIXTURE_DEF.filter.maskBits = CollisionType.PLAYER;

            Body body = world.createBody(WALL_BODY_DEF);
            trackedBodies.add(body);
            body.createFixture(WALL_FIXTURE_DEF)
                    .getFilterData();
        }
    }
}
