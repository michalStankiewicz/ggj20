package org.mssg.ggj20.game;

import com.badlogic.gdx.math.MathUtils;

public class Difficulty {
    private int level;
    private int levelTreshold;
    private int score;
    private int problemsGenerated;
    private int problemsSolved;

    public Difficulty() {
    }

    public void reset() {
        problemsGenerated = 0;
        level = 1;
        levelTreshold = 4;
        problemsSolved = 0;
    }

    public boolean noMoreProblems() {
        return problemsGenerated == levelTreshold;
    }

    public int getScore() {
        return score;
    }

    public void scorePlusPlus() {
        score++;
        problemsSolved++;
    }

    public void addProblem() {
        problemsGenerated++;
    }

    public long getCoolDown() {
//        return 120;
        return (int) (Math.random() * 100) + 150 + MathUtils.clamp(100 - level, 0, 100) * 2;
    }

    public boolean isThereAProblem() {
//        return true;
        return Math.random() < 0.005f + (0.03 * level / 100);
    }

    public boolean levelCompleted() {
        return problemsSolved == levelTreshold;
    }

    public int getLevel() {
        return level;
    }

    public void levelUp() {
        level++;
        levelTreshold = 2 + level * 2;
        problemsSolved = 0;
        problemsGenerated = 0;
    }

    @Override
    public String toString() {
        return "Difficulty{" +
                "level=" + level +
                ", levelTreshold=" + levelTreshold +
                ", score=" + score +
                ", problemsGenerated=" + problemsGenerated +
                ", problemsSolved=" + problemsSolved +
                '}';
    }
}
