package org.mssg.ggj20.game;

class SinglePlayerInput {
    private byte controls;

    public SinglePlayerInput(byte controls) {
        this.controls = controls;
    }

    public byte getControls() {
        return controls;
    }

    public void update(byte controls) {
        this.controls = controls;
    }
}
