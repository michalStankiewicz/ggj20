package org.mssg.ggj20.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import org.mssg.ggj20.assets.GFX;
import org.mssg.ggj20.screens.game.entities.CollisionType;

public class ItGuyEntity {
    public static FixtureDef IT_GUY_FIXTURE_DEF = new FixtureDef();
    public static BodyDef IT_GUY_BODY_DEF = new BodyDef();
    private final Sprite help;
    private final Sprite donutPlease;
    private final Sprite iHaveBug;
    private final Sprite noPower;
    private final Sprite thankyou;
    private final Sprite guy;
    private final Sprite worriedGuy;
    float modifier;
    float thankyouTimeout = 0;
    private Body body;
    private int id;
    private boolean needsHelp;
    private HelpDetails problem;
    private boolean isWorried;
    private long tickThresholdBeforeProblem;

    public ItGuyEntity(int itGuyId) {
        id = itGuyId;
        help = new Sprite(GFX.QUESTION_MARK);
        donutPlease = new Sprite(GFX.DONUT);
        iHaveBug = new Sprite(GFX.BUG);
        noPower = new Sprite(GFX.NO_POWER);
        thankyou = new Sprite(GFX.THANK_YOU[0]);

        help.setScale(0.02f);
        donutPlease.setScale(0.02f);
        iHaveBug.setScale(0.02f);
        noPower.setScale(0.02f);
        thankyou.setScale(0.02f);
        modifier = (float) (Math.random() * 3.1415);
        problem = HelpDetails.UNKNOWN;
        isWorried = false;

        int imageId = id%GFX.IT_GUYS.length;
        boolean flip = Math.random() < 0.5f;

        guy = new Sprite(GFX.IT_GUYS[imageId]);
        worriedGuy = new Sprite(GFX.IT_GUYS_PROBLEM[imageId]);

        guy.setScale(0.02f);
        worriedGuy.setScale(0.02f);

        if(flip){
            guy.flip(true, false);
            worriedGuy.flip(true, false);
        }
    }

    public static void init() {
        IT_GUY_BODY_DEF.type = BodyDef.BodyType.StaticBody;

        CircleShape circle = new CircleShape();
        circle.setRadius(1);
        IT_GUY_FIXTURE_DEF.shape = circle;

        IT_GUY_FIXTURE_DEF.filter.categoryBits = CollisionType.IT_GUY;
        IT_GUY_FIXTURE_DEF.filter.maskBits = CollisionType.PLAYER;
        IT_GUY_FIXTURE_DEF.isSensor = true;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public void setPosition(float x, float y) {
        body.setTransform(x, y, 0);
        help.setCenter(x, y);
        donutPlease.setCenter(x, y);
        iHaveBug.setCenter(x, y);
        noPower.setCenter(x, y);
        thankyou.setCenter(x, y);

        guy.setCenter(x, y);

        worriedGuy.setCenter(x, y);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setNeedsHelp(boolean needsHelp) {
        this.needsHelp = needsHelp;
    }

    public boolean needsHelp() {
        return needsHelp;
    }

    public void render(float delta, float uptime, SpriteBatch batch) {
        if (needsHelp) {
            if (Math.random() < 0.005f) {
                isWorried = !isWorried;
            }

            if (isWorried) {
                worriedGuy.draw(batch);
            } else {
                guy.draw(batch);
            }

            switch (problem) {
                case BUG:
                    iHaveBug.setCenter(body.getPosition().x, (float) (body.getPosition().y + 1 + Math.sin((uptime + modifier) * 4) * 0.3f));
                    iHaveBug.draw(batch);
                    break;
                case DONUT:
                    donutPlease.setCenter(body.getPosition().x, (float) (body.getPosition().y + 1 + Math.sin((uptime + modifier) * 4) * 0.3f));
                    donutPlease.draw(batch);
                    break;
                case COMPUTER:
                    noPower.setCenter(body.getPosition().x, (float) (body.getPosition().y + 1 + Math.sin((uptime + modifier) * 4) * 0.3f));
                    noPower.draw(batch);
                    break;
                case UNKNOWN:
                    help.setCenter(body.getPosition().x, (float) (body.getPosition().y + 1 + Math.sin((uptime + modifier) * 4) * 0.3f));
                    help.draw(batch);
                    break;
            }

        } else if (thankyouTimeout > 0) {
            thankyouTimeout -= delta;
            thankyou.setCenter(body.getPosition().x, (float) (body.getPosition().y + 1 + Math.sin((uptime + modifier) * 4) * 0.3f));
            thankyou.draw(batch);
            guy.draw(batch);
        } else {
            guy.draw(batch);
        }
    }

    public HelpDetails getProblem() {
        return problem;
    }

    public void setProblem(HelpDetails problem) {
        this.problem = problem;
        if (problem == null) {
            thankyouTimeout = 3;
            thankyou.setTexture(GFX.THANK_YOU[(int) (Math.random() * 3)]);
            this.problem = HelpDetails.UNKNOWN;
            needsHelp = false;
            return;
        }
    }

    public long getTickThresholdBeforeProblem() {
        return tickThresholdBeforeProblem;
    }

    public void setTickThresholdBeforeProblem(long tickThresholdBeforeProblem) {
        this.tickThresholdBeforeProblem = tickThresholdBeforeProblem;
    }
}
