package org.mssg.ggj20.game;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.World;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.assets.MAPS;
import org.mssg.ggj20.assets.SND;
import org.mssg.ggj20.game.input.NetwordControlsCode;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.BoardMoodUpdateEvent;
import org.mssg.ggj20.network.game.events.GameOverEvent;
import org.mssg.ggj20.network.game.events.ItGuyNeedsHelpEvent;
import org.mssg.ggj20.network.game.events.LevelUpEvent;
import org.mssg.ggj20.network.game.events.PlayerAskedToHelpEvent;
import org.mssg.ggj20.network.game.events.PlayerInputEvent;
import org.mssg.ggj20.network.game.events.PlayerSolvedProblem;
import org.mssg.ggj20.network.game.events.PlayerUpdateEvent;
import org.mssg.ggj20.network.game.events.TickUpdateEvent;
import org.mssg.ggj20.network.game.server.PingPongArray;
import org.mssg.ggj20.screens.game.HUD;
import org.mssg.ggj20.screens.game.entities.PlayerEntity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ConcurrentMap;

public class GameState {

    public static final float TICK_RATE = CFG.getInt("multiplayer.tickMillis") / 1000f;
    public static final int STARTING_BOARD_MOOD = CFG.getInt("boardMoodStart");
    private static final int PIX_PER_TILE = CFG.getInt("tileset.pixPerTile");
    MapLoader mapLoader = new MapLoader();

    private ConcurrentMap<Integer, Player> players;
    private ConcurrentMap<Integer, PingPongArray> pingPong;
    private ConcurrentMap<Integer, PlayerInput> playerInputs;

    private ConcurrentLinkedQueue<PlayerInput> playerInputQueue;
    private List<PlayerInput> playerInputBuffer;

    //    private ConcurrentMap<Integer, PlayerEntity> playerEntities;
    private ConcurrentMap<Integer, PlayerUpdateEvent> playerUpdates;

    private List<Body> trackedBodies = new ArrayList<>();

    private List<Vector2> spawns = new ArrayList<>();
    private String me;
    private int myId;
    private boolean gameOn;
    private List<PlayerEntity> playerEntities = new ArrayList<>();
    private Map<Integer, PlayerEntity> playerEntityPerId = new HashMap<>();
    private long lastPersistedTick = 0;
    private Map<Integer, SinglePlayerInput> inputForCurrentTick;
    private Map<Integer, SinglePlayerInput> lastControlsForPlayer;
    private World world;
    private ConcurrentMap<Integer, ItGuyEntity> itGuysById;
    private List<ItGuyEntity> itGuys;
    private int score;
    private int boardMood;
    private Difficulty difficulty;

    public GameState() {
        players = new ConcurrentHashMap<>();
        pingPong = new ConcurrentHashMap<>();
        playerInputs = new ConcurrentHashMap<>();
        playerInputQueue = new ConcurrentLinkedQueue<>();
        playerInputBuffer = new ArrayList<>();
        inputForCurrentTick = new HashMap<>();
        lastControlsForPlayer = new HashMap<>();
        playerUpdates = new ConcurrentHashMap<>();
        itGuysById = new ConcurrentHashMap<>();
        itGuys = new ArrayList<>();
        difficulty = new Difficulty();
        reset();
    }

    public void reset() {
        gameOn = false;
        for (int i = 0; i < trackedBodies.size(); i++) {
            world.destroyBody(trackedBodies.get(i));
        }
        trackedBodies.clear();
        players.clear();
        pingPong.clear();
        score = 0;
        boardMood = STARTING_BOARD_MOOD;
        difficulty.reset();
    }

    public void preparePhysics() {
        this.world = new World(Vector2.Zero, true);
        mapLoader.loadMap(MAPS.MAP, this.world, trackedBodies);
        addSpawns(MAPS.MAP, "SPAWNS");
        addItGuys(MAPS.MAP, world, "IT_GUYS");
        addPlayers(world, GameServerController.getGameState().getPlayers().values().toArray(new Player[0]),
                GameServerController.getGameState());
        world.setContactListener(new GenericContactListener());
    }

    public void prepareWorld(TiledMap map, World world) {
        this.world = world;
        mapLoader.loadMap(map, world, trackedBodies);
        addSpawns(map, "SPAWNS");
        addItGuys(MAPS.MAP, world, "IT_GUYS");
        addPlayers(world, GameClientController.getGameState().getPlayers().values().toArray(new Player[0]),
                GameClientController.getGameState());
    }

    private void addItGuys(TiledMap map, World world, String layer) {
        MapLayer itGuysLayer = map.getLayers().get(layer);

        int itGuyId = 0;
        for (MapObject itGuy : itGuysLayer.getObjects()) {
            float x = (float) itGuy.getProperties().get("x") / (PIX_PER_TILE) + 0.5f;
            float y = (float) itGuy.getProperties().get("y") / (PIX_PER_TILE) + 0.5f;

            ItGuyEntity itGuyEntity = new ItGuyEntity(itGuyId);

            Body itGuyBody = world.createBody(ItGuyEntity.IT_GUY_BODY_DEF);
            itGuyEntity.setBody(itGuyBody);
            Fixture fixture = itGuyBody.createFixture(ItGuyEntity.IT_GUY_FIXTURE_DEF);
            fixture.setUserData(itGuyEntity);

            itGuyEntity.setPosition(x, y);


            trackedBodies.add(itGuyBody);

            itGuysById.put(itGuyId, itGuyEntity);
            itGuys.add(itGuyEntity);

            itGuyId++;
        }
    }

    private void addSpawns(TiledMap map, String layer) {
        MapLayer spawnsLayer = map.getLayers().get(layer);
        for (MapObject spawn : spawnsLayer.getObjects()) {
            float x = (float) spawn.getProperties().get("x") / (PIX_PER_TILE) + 0.5f;
            float y = (float) spawn.getProperties().get("y") / (PIX_PER_TILE) + 0.5f;

            spawns.add(new Vector2(x, y));
        }
    }

    public void addPlayers(World world, Player[] players, GameState targetGameState) {
        for (int i = 0; i < players.length; i++) {
            Player player = players[i];

            PlayerEntity playerEntity = new PlayerEntity();
            playerEntity.setOwnerId(player.getId());
            playerEntity.setCharacter(player.getCharacter());

            Body playerBody = world.createBody(PlayerEntity.PLAYER_BODY_DEF);
            playerEntity.setBody(playerBody);
            Fixture fixture = playerBody.createFixture(PlayerEntity.PLAYER_FIXTURE_DEF);
            fixture.setUserData(playerEntity);

            Vector2 spawnPoint = getRandom(spawns);
            playerEntity.setPosition(spawnPoint.x, spawnPoint.y);

            targetGameState.addPlayerEntity(playerEntity);
            trackedBodies.add(playerBody);
        }
    }

    private Vector2 getRandom(List<Vector2> spawns) {
        return spawns.get((int) (Math.random() * spawns.size()));
    }

    public void addPlayer(int id, Player player) {
        players.put(id, player);
        pingPong.put(id, new PingPongArray());
        lastControlsForPlayer.put(id, new SinglePlayerInput((byte) 0));
        if (me != null && me.equals(player.getName())) {
            setMyId(id);
        }
    }

    public void removePlayer(int id) {
        players.remove(id);
        pingPong.remove(id);
        lastControlsForPlayer.remove(id);
    }

    public ConcurrentMap<Integer, Player> getPlayers() {
        return players;
    }

    public void updatePlayers(Map<Integer, Player> players) {
        for (int id : this.players.keySet()) {
            if (!players.containsKey(id)) {
                this.players.remove(id);
                this.pingPong.remove(id);
                this.lastControlsForPlayer.remove(id);
            }
        }

        for (int id : players.keySet()) {
            if (!this.players.containsKey(id)) {
                this.players.put(id, players.get(id));
                this.pingPong.put(id, new PingPongArray());
                this.lastControlsForPlayer.put(id, new SinglePlayerInput((byte) 0));
            } else {
                this.players.get(id).setCharacter(players.get(id).getCharacter());
            }
        }
    }

    public String getMe() {
        return me;
    }

    public void setMe(String me) {
        this.me = me;
    }

    public int getMyId() {
        return myId;
    }

    public void setMyId(int id) {
        this.myId = id;
    }

    public boolean isGameOn() {
        return gameOn;
    }

    public void setGameOn(boolean gameOn) {
        this.gameOn = gameOn;
    }

    public ConcurrentMap<Integer, PingPongArray> getPingPong() {
        return pingPong;
    }

    public void syncTick() {
        for (int id : pingPong.keySet()) {
            TickUpdateEvent currentTickEvent = new TickUpdateEvent();
            long tick = GameServerController.getTickCounter().getTick();
            currentTickEvent.setTick(pingPong.get(id).calculateDelay() + tick);
            GameServerController.getServer().sendToTCP(id, currentTickEvent);
        }
    }

    public void bufferPlayerUpdate(PlayerUpdateEvent playerUpdateEvent) {
        int id = playerUpdateEvent.getId();

        if (null == playerUpdates.get(id) ||
                playerUpdates.get(id).getTick() < playerUpdateEvent.getTick()) {
            playerUpdates.put(id, playerUpdateEvent);
        }
    }

    public void bufferInput(int id, PlayerInputEvent playerInputEvent) {
        playerInputQueue.add(new PlayerInput(id, playerInputEvent));
    }

    public void simulateTicks(long currentTick) {
        if (gameOn) {
            simulateTicksInternal(currentTick);
        }
    }

    private void simulateTicksInternal(long currentTick) {
        long startingTick = lastPersistedTick;

        dequeuePlayerInputs(startingTick);
        clearAnyHelp();

        for (long tick = startingTick; tick <= currentTick; tick++) {
            if (collectInputForTick(tick)) {
//                restoreWorld();
                simulateTick(tick);
//                persistWorld();
                lastPersistedTick = tick;
            } else {
//                simulateTick(tick);
            }
        }

        if (difficulty.levelCompleted()) {
            boardMood = STARTING_BOARD_MOOD;
            difficulty.levelUp();
            GameServerController.getServer().sendToAllTCP(new LevelUpEvent(difficulty.getLevel()));
        }

        int boardMoodPrev = boardMood;
        boardMood -= guysWhoNeedHelp();

        if (moodDroppedBelow(boardMoodPrev, boardMood, (int) (STARTING_BOARD_MOOD * 0.75f))) {
            GameServerController.getServer().sendToAllTCP(new BoardMoodUpdateEvent(BoardMood.STILL_OK));
        }
        if (moodDroppedBelow(boardMoodPrev, boardMood, (int) (STARTING_BOARD_MOOD * 0.5f))) {
            GameServerController.getServer().sendToAllTCP(new BoardMoodUpdateEvent(BoardMood.WORRIED));
        }
        if (moodDroppedBelow(boardMoodPrev, boardMood, (int) (STARTING_BOARD_MOOD * 0.25f))) {
            GameServerController.getServer().sendToAllTCP(new BoardMoodUpdateEvent(BoardMood.ANGRY));
        }
        if (moodDroppedBelow(boardMoodPrev, boardMood, (int) (STARTING_BOARD_MOOD * 0.1f))) {
            GameServerController.getServer().sendToAllTCP(new BoardMoodUpdateEvent(BoardMood.FURIOUS));
        }
        if (boardMood < 0) {
            GameServerController.getServer().sendToAllTCP(new GameOverEvent(difficulty.getScore()));
        }

        sendUpdates(currentTick);
    }

    private boolean moodDroppedBelow(int boardMoodPrev, int boardMood, int treshold) {
        return boardMoodPrev > treshold && boardMood <= treshold;
    }

    private void clearAnyHelp() {
        for (int id : playerEntityPerId.keySet()) {
            PlayerEntity playerEntity = playerEntityPerId.get(id);
            playerEntity.askedToHelp(null);
            playerEntity.solvedProblem(null);
        }
    }

    private void sendUpdates(long tick) {
        for (int id : playerEntityPerId.keySet()) {
            PlayerUpdateEvent playerUpdateEvent = new PlayerUpdateEvent();

            playerUpdateEvent.setTick(lastPersistedTick);
            playerUpdateEvent.setId(id);
            playerUpdateEvent.setControls(lastControlsForPlayer.get(id).getControls());

            PlayerEntity playerEntity = playerEntityPerId.get(id);
            Vector2 position = playerEntity.getPosition();
            playerUpdateEvent.setX(position.x);
            playerUpdateEvent.setY(position.y);

            if (playerEntity.getAskedToHelp() != null && playerEntity.getItGuyWhoIHelped() == null) {
                GameServerController.getServer().sendToAllTCP(new PlayerAskedToHelpEvent(playerEntity.getAskedToHelp(),
                        itGuysById.get(playerEntity.getAskedToHelp()).getProblem()));
            }

            if (playerEntity.getItGuyWhoIHelped() != null) {
                GameServerController.getGameState().incrementScore();
                GameServerController.getServer().sendToAllTCP(new PlayerSolvedProblem(playerEntity.getOwnerId(),
                        playerEntity.getItGuyWhoIHelped(),
                        GameServerController.getGameState().getScore()));
            }

            GameServerController.getServer().sendToAllUDP(playerUpdateEvent);
        }
    }

    private int getScore() {
        return difficulty.getScore();
    }

    private void incrementScore() {
        difficulty.scorePlusPlus();
    }

    private void dequeuePlayerInputs(long tick) {
        playerInputBuffer.removeIf(input -> input.getFirstTick() + 7 < tick);
        while (true) {
            PlayerInput input = playerInputQueue.poll();
            if (null == input) {
                return;
            } else {
                playerInputBuffer.add(input);
            }
        }
    }

    private boolean collectInputForTick(long tick) {
        boolean allActual = true;
        inputForCurrentTick.clear();

        for (int i = 0; i < playerInputBuffer.size(); i++) {
            PlayerInput input = playerInputBuffer.get(i);

            if (inputForCurrentTick.containsKey(input.getPlayerId())) {
                continue;
            }

            if (input.getFirstTick() < tick && input.getFirstTick() + 7 > tick) {
                if (inputForCurrentTick.containsKey(input.getPlayerId())) {
                    inputForCurrentTick.get(input.getPlayerId()).update(input.getForTick(tick));
                } else {
                    inputForCurrentTick.put(input.getPlayerId(), new SinglePlayerInput(input.getForTick(tick)));
                }
                lastControlsForPlayer.put(input.getPlayerId(), inputForCurrentTick.get(input.getPlayerId()));
            }
        }

        Integer[] playerIds = players.keySet().toArray(new Integer[0]);

        for (int i = 0; i < playerIds.length; i++) {
            if (!inputForCurrentTick.containsKey(playerIds[i])) {
                allActual = false;
                inputForCurrentTick.put(playerIds[i], lastControlsForPlayer.get(playerIds[i]));
            }
        }

        return allActual;
    }

    private void simulateTick(long tick) {
        checkIfItGuyNeedsHelp(tick);

        //applyControls
        for (int key : inputForCurrentTick.keySet()) {
            byte controls = inputForCurrentTick.get(key).getControls();

            boolean up = (controls & NetwordControlsCode.UP) > 0;
            boolean down = (controls & NetwordControlsCode.DOWN) > 0;
            boolean left = (controls & NetwordControlsCode.LEFT) > 0;
            boolean right = (controls & NetwordControlsCode.RIGHT) > 0;
            boolean action = (controls & NetwordControlsCode.ACTION_1) > 0;

            PlayerEntity player = playerEntityPerId.get(key);
            player.applyControls(up, down, left, right);

            Integer nearItGuy = player.getNearItGuy();


            if (action && nearItGuy != null && itGuysById.get(nearItGuy).needsHelp()) {
                ItGuyEntity itGuy = itGuysById.get(nearItGuy);
                if (itGuy.getProblem() == HelpDetails.UNKNOWN) {
                    player.askedToHelp(nearItGuy);
                    itGuy.setProblem(HelpDetails.random());
                    if (player.getCharacter().canHelpWith(itGuy.getProblem())) {
                        player.solvedProblem(nearItGuy);
                        itGuy.setNeedsHelp(false);
                        itGuy.setProblem(HelpDetails.UNKNOWN);
                        itGuy.setTickThresholdBeforeProblem(tick + difficulty.getCoolDown());
                    }
                } else if (player.getCharacter().canHelpWith(itGuy.getProblem())) {
                    player.solvedProblem(nearItGuy);
                    itGuy.setNeedsHelp(false);
                    itGuy.setProblem(HelpDetails.UNKNOWN);
                    itGuy.setTickThresholdBeforeProblem(tick + difficulty.getCoolDown());
                }
            }
        }

        //simulate
        world.step(TICK_RATE, 3, 2);

    }

    private int guysWhoNeedHelp() {
        int problems = 0;
        for (int i = 0; i < itGuys.size(); i -= -1) {
            if (itGuys.get(i).needsHelp()) {
                problems++;
            }
        }
        return problems;
    }

    private void checkIfItGuyNeedsHelp(long tick) {
        if (difficulty.noMoreProblems()) {
            return;
        }

        if (difficulty.isThereAProblem()) {
            int itGuyId = (int) (Math.random() * itGuysById.size());
            ItGuyEntity itGuy = itGuysById.get(itGuyId);

            if (!itGuy.needsHelp() && tick > itGuy.getTickThresholdBeforeProblem()) {
                difficulty.addProblem();
                itGuy.setNeedsHelp(true);
                GameServerController.getServer().sendToAllTCP(new ItGuyNeedsHelpEvent(itGuyId));
            }
        }
    }

    public void addPlayerEntity(PlayerEntity playerEntity) {
        playerEntities.add(playerEntity);
        playerEntityPerId.put(playerEntity.getOwnerId(), playerEntity);
    }

    public void clearPlayerEntities() {
        playerEntities.clear();
        playerEntityPerId.clear();
    }

    public PlayerEntity getMyPlayerEntity() {
        return playerEntityPerId.get(GameClientController.getGameState().getMyId());
    }

    public List<PlayerEntity> getPlayerEntities() {
        return playerEntities;
    }

    public void updateState() {
        for (int id : playerUpdates.keySet()) {
            PlayerUpdateEvent playerUpdateEvent = playerUpdates.get(id);

            float x = playerUpdateEvent.getX();
            float y = playerUpdateEvent.getY();

            PlayerEntity player = playerEntityPerId.get(id);

//            float r = 0;
//            if (player.getBody() != null) {
//                r = player.getBody().getAngle();
//            }

            byte controls = playerUpdateEvent.getControls();

            boolean up = (controls & NetwordControlsCode.UP) > 0;
            boolean down = (controls & NetwordControlsCode.DOWN) > 0;
            boolean left = (controls & NetwordControlsCode.LEFT) > 0;
            boolean right = (controls & NetwordControlsCode.RIGHT) > 0;

            player.setLastControls(up, down, left, right);
            player.setPosition(x, y);

//            if(playerUpdates.get(id).getFirstTick() < tickTmp){
//                tickTmp = playerUpdates.get(id).getFirstTick();
//            }
        }
    }

    public void markItGuysNeedHelp(int itGuyId) {
        itGuysById.get(itGuyId).setNeedsHelp(true);
        for (int i = 0; i < playerEntities.size(); i++) {
            playerEntities.get(i).addRadarMarkerFor(itGuys.get(itGuyId));
        }
    }

    public List<ItGuyEntity> getItGuys() {
        return itGuys;
    }

    public ConcurrentMap<Integer, ItGuyEntity> getItGuysById() {
        return itGuysById;
    }

    public void itGuyNeedsSpecificHelp(Integer itGuyId, HelpDetails problem) {
        itGuysById.get(itGuyId).setProblem(problem);
        for (int i = 0; i < playerEntities.size(); i++) {
            PlayerEntity playerEntity = playerEntities.get(i);

            if (playerEntity.getCharacter().canHelpWith(problem)) {
                playerEntity.advanceRadar(itGuyId);
                if (getMyId() == playerEntity.getOwnerId()) {
                    HUD.get().displayProblemInfo(playerEntity.getCharacter());
                    SND.play(SND.NEW_PROBLEM);
                }
            } else {
                playerEntity.removeRadar(itGuyId);
            }
        }
    }

    public void problemSolved(PlayerSolvedProblem problemSolved) {
        itGuysById.get(problemSolved.getHappyItGuyId()).setProblem(null);

        for (int i = 0; i < playerEntities.size(); i++) {
            PlayerEntity playerEntity = playerEntities.get(i);
            playerEntity.removeRadar(problemSolved.getHappyItGuyId());
        }

        score = problemSolved.getScore();

        if (problemSolved.getPlayerId() == getMyId()) {
            switch (getMyPlayerEntity().getCharacter()) {
                case COOKIE_JAR:
                    SND.play(SND.COMPUTER_PROBLEM_FIXED);
                    break;
                case OCTO_PIE:
                    SND.play(SND.BUG_PROBLEM_FIXED);
                    break;
                case DONUT_RAPTOR:
                    SND.play(SND.DONUT_PROBLEM_FIXED);
                    break;
            }
        }
    }
}
