package org.mssg.ggj20.game;

import org.mssg.ggj20.screens.lobby.PlayerCharacter;

public class Player {
    private String name;
    private int id;
    private boolean ready;
    private PlayerCharacter character = PlayerCharacter.NONE;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public PlayerCharacter getCharacter() {
        return character;
    }

    public void setCharacter(PlayerCharacter character) {
        this.character = character;
    }
}
