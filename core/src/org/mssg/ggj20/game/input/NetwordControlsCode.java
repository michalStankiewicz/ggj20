package org.mssg.ggj20.game.input;

public class NetwordControlsCode {

    public static final transient byte UP = 0b1;
    public static final transient byte DOWN = 0b10;
    public static final transient byte LEFT = 0b100;
    public static final transient byte RIGHT = 0b1000;
    public static final transient byte ACTION_1 = 0b1_0000;

}
