package org.mssg.ggj20.game.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;

import java.util.BitSet;

public class InputMonitor extends InputAdapter {

    private static final BitSet keyStatusChanged;

    static {
        keyStatusChanged = new BitSet();
        keyStatusChanged.clear();
    }

    public static byte collect() {

        byte controls = 0;

        if (wasKeyUsedThisTick(Input.Keys.UP)) {
            controls |= NetwordControlsCode.UP;
        }

        if (wasKeyUsedThisTick(Input.Keys.DOWN)) {
            controls |= NetwordControlsCode.DOWN;
        }

        if (wasKeyUsedThisTick(Input.Keys.LEFT)) {
            controls |= NetwordControlsCode.LEFT;
        }

        if (wasKeyUsedThisTick(Input.Keys.RIGHT)) {
            controls |= NetwordControlsCode.RIGHT;
        }

        if (wasKeyUsedThisTick(Input.Keys.SPACE)) {
            controls |= NetwordControlsCode.ACTION_1;
        }

        keyStatusChanged.clear();
        return controls;
    }

    private static boolean wasKeyUsedThisTick(int keycode) {
        return Gdx.input.isKeyPressed(keycode) || keyStatusChanged.get(keycode);
    }

    @Override
    public boolean keyDown(int keycode) {
        keyStatusChanged.set(keycode);
        return true;
    }

    @Override
    public boolean keyUp(int keycode) {
        keyStatusChanged.set(keycode);
        return true;
    }
}
