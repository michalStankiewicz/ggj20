package org.mssg.ggj20.game;

public enum BoardMood {
    HAPPY,
    STILL_OK,
    WORRIED,
    ANGRY,
    FURIOUS
}
