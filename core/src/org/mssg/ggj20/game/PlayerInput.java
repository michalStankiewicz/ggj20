package org.mssg.ggj20.game;

import org.mssg.ggj20.network.game.events.PlayerInputEvent;

class PlayerInput {

    private int playerId;
    private byte[] controls;
    private long firstTick;

    public PlayerInput(int id, PlayerInputEvent playerInputEvent) {
        playerId = id;
        controls = playerInputEvent.getControls();
        firstTick = playerInputEvent.getFirstTick();
    }

    public int getPlayerId() {
        return playerId;
    }

    public byte[] getControls() {
        return controls;
    }

    public long getFirstTick() {
        return firstTick;
    }

    public byte getForTick(long tick) {
        return controls[(int) (tick - firstTick)];
    }
}
