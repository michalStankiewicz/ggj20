package org.mssg.ggj20.game;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import org.mssg.ggj20.screens.game.entities.PlayerEntity;

class GenericContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Object data1 = contact.getFixtureA().getUserData();
        Object data2 = contact.getFixtureB().getUserData();
        checkTouch(data1, data2);
        checkTouch(data2, data1);

    }

    private void checkTouch(Object data1, Object data2) {
        if (data1 instanceof PlayerEntity
                && data2 instanceof ItGuyEntity) {
            PlayerEntity playerEntity = (PlayerEntity) data1;
            ItGuyEntity itGuy = (ItGuyEntity) data2;
            playerEntity.setNearItGuy(itGuy.getId());
        }
    }

    @Override
    public void endContact(Contact contact) {
        Object data1 = contact.getFixtureA().getUserData();
        Object data2 = contact.getFixtureB().getUserData();

        checkUntouch(data1, data2);
        checkUntouch(data2, data1);
    }


    private void checkUntouch(Object data1, Object data2) {
        if (data1 instanceof PlayerEntity
                && data2 instanceof ItGuyEntity) {
            PlayerEntity playerEntity = (PlayerEntity) data1;
            playerEntity.setNearItGuy(null);
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}
