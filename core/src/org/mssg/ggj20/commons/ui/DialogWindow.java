package org.mssg.ggj20.commons.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.screens.mainmenu.actions.GameMenuAction;

public class DialogWindow extends Dialog {

    private static final DialogWindow THIS = new DialogWindow();

    private Label questionLabel;
    private TextButton yesButton;
    private TextButton noButton;
    private TextButton okButton;
    private GameMenuAction action;

    private DialogWindow() {
        super(I18N.get("confirmDialog.title"), UI.SHADE_SKIN);

        setSize(CFG.getInt("defaultDialogWindow.width"),
                CFG.getInt("defaultDialogWindow.height"));

        questionLabel = new Label("", UI.SHADE_SKIN);
        yesButton = new TextButton(I18N.get("generic.yes"), UI.SHADE_SKIN);
        noButton = new TextButton(I18N.get("generic.no"), UI.SHADE_SKIN);
        okButton = new TextButton(I18N.get("generic.ok"), UI.SHADE_SKIN);
        getContentTable().add(questionLabel);

        row();

        addListeners();
    }

    public static Dialog getConfirmActionWindow(String question, GameMenuAction action) {

        THIS.getTitleLabel().setText(I18N.get("confirmDialog.confirmTitle"));
        THIS.getButtonTable().clear();
        THIS.questionLabel.setText(question);
        THIS.action = action;

        THIS.addYesButton();
        THIS.addNoButton();

        return THIS;
    }

    public static DialogWindow getErrorDialog(String error, GameMenuAction action) {
        THIS.getTitleLabel().setText(I18N.get("confirmDialog.errorTitle"));
        THIS.getButtonTable().clear();
        THIS.questionLabel.setText(error);
        THIS.action = action;

        THIS.addOkButton();

        return THIS;
    }

    private void addNoButton() {
        getButtonTable().add(noButton);
    }

    private void addYesButton() {
        getButtonTable().add(yesButton);
    }

    private void addOkButton() {
        getButtonTable().add(okButton);
    }

    private void addListeners() {
        yesButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                DialogWindow.this.remove();
                callAction();
            }
        });

        noButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                makeStageActorsTouchable();
                DialogWindow.this.remove();
            }

            private void makeStageActorsTouchable() {
                for (int i = 0; i < getStage().getActors().size; i++) {
                    Actor actor = getStage().getActors().items[i];
                    if (actor.isVisible() && actor.getTouchable().equals(Touchable.disabled)) {
                        actor.setTouchable(Touchable.enabled);
                    }
                }
            }
        });

        okButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                makeStageActorsTouchable();
                DialogWindow.this.remove();
            }

            private void makeStageActorsTouchable() {
                for (int i = 0; i < getStage().getActors().size; i++) {
                    Actor actor = getStage().getActors().items[i];
                    if (actor.isVisible() && actor.getTouchable().equals(Touchable.disabled)) {
                        actor.setTouchable(Touchable.enabled);
                    }
                }
            }
        });
    }

    private void callAction() {
        this.action.doAction();
        this.action = null;
    }
}
