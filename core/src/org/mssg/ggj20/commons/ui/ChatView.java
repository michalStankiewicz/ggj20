package org.mssg.ggj20.commons.ui;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextArea;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.GameServerController;
import org.mssg.ggj20.network.game.events.PlayerSendMessageEvent;
import org.mssg.ggj20.screens.mainmenu.actions.GameMenuAction;

public class ChatView extends Table {

    private static ChatView THIS;
    private final Window playersListWindow;

    private Stage parentStage;
    private TextArea chatTextArea;
    private TextField chatTextBox;
    private TextButton sendMessageButton;
    private TextButton leaveGameButton;


    private ChatView(Skin skin) {
        super(skin);
//        setBackground(new SpriteDrawable(new Sprite(GraphicAssets.get().background)));

        setFillParent(true);
//        setDebug(true);

        chatTextArea = new TextArea("", skin);
        chatTextArea.setPrefRows(6);
        chatTextArea.setDisabled(true);

        chatTextBox = new TextField("", skin);
        chatTextBox.setMaxLength(200);

        sendMessageButton = new TextButton(I18N.get("chatView.sendMessageButton.text"), skin);
        leaveGameButton = new TextButton(I18N.get("chatView.quitGameButton.text"), skin);
        playersListWindow = new Window(I18N.get("chatView.playersList.title"), skin);

//        playersListWindow.setMovable(false);
//        add(playersListWindow).growY().width(200);
        add().colspan(3).expandY();//.grow();
        row();

        add(chatTextArea).colspan(3).expandX().fill();
        row();

        add(chatTextBox).expandX().fill();
        add(sendMessageButton);
        add(leaveGameButton);

        addListeners();
    }

    public static void addChatMessage(String message) {
        get().chatTextArea.appendText(message);
        get().chatTextArea.appendText("\n");
    }

    public static ChatView getOnStage(Stage parentStage) {
        ChatView chatView = get();
        chatView.parentStage = parentStage;
        return chatView;
    }

    public static ChatView get() {
        if (THIS == null) {
            THIS = new ChatView(UI.SHADE_SKIN);
        }
        return THIS;
    }

    private void addListeners() {
        chatTextBox.setFocusTraversal(false);
        chatTextBox.setTextFieldListener((textField, key) -> {
            if ((key == '\r' || key == '\n')) {
                sendMessage(textField);
            }
        });

        sendMessageButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (chatTextBox.getText().length() == 0) {
                    return;
                }
                sendMessage(chatTextBox);
            }
        });

        leaveGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (GameServerController.amIHost()) {
                    DialogWindow.getConfirmActionWindow(
                            "You are host. Are you sure?",
                            GameMenuAction.UNHOST_GAME_ACTION)
                            .show(getStage());
                } else {
                    DialogWindow.getConfirmActionWindow(
                            "Are you sure?",
                            GameMenuAction.LEAVE_GAME_ACTION)
                            .show(getStage());
                }
            }
        });
    }

    private void sendMessage(TextField textField) {
        PlayerSendMessageEvent messageEvent = new PlayerSendMessageEvent();
        messageEvent.setPlayer(GameClientController.getGameState().getMe());
        messageEvent.setMessage(textField.getText());
        GameClientController.getClient().sendTCP(messageEvent);
        textField.setText("");
    }

    public void clearChat() {
        chatTextArea.setText("");
    }
}
