package org.mssg.ggj20.screens.gameover;

import com.badlogic.gdx.scenes.scene2d.Stage;

import org.mssg.ggj20.assets.UI;

public class GameOverStage extends Stage {

    private static GameOverStage THIS;
    private GameOverView gameOverView;
    private BSODView bsodView;

    GameOverStage() {
        THIS = this;
        gameOverView = new GameOverView(UI.SHADE_SKIN, this);
        bsodView = new BSODView(UI.SHADE_SKIN, this);
        addActor(bsodView);
        addActor(gameOverView);
    }

    public static GameOverStage getInstance() {
        return THIS;
    }


}
