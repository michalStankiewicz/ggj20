package org.mssg.ggj20.screens.gameover;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import org.mssg.ggj20.assets.GFX;

public class BSODView extends Table {

    private Image bsod;


    public BSODView(Skin skin, Stage stage) {
        super(skin);

        setFillParent(true);

        bsod = new Image();
        bsod = new Image(GFX.BSOD);
        bsod.setScaling(Scaling.fit);

        add();
        add();
        add();
        row();

        add();

        float height = Gdx.graphics.getHeight() * 0.9f;
        float width = Gdx.graphics.getWidth() * 0.9f;
        add(bsod).prefHeight(height).prefWidth(width).center().align(Align.center);
        add();
        row();

        add();
        add();
        add();


    }

}
