package org.mssg.ggj20.screens.gameover;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.GameServerController;

public class GameOverView extends Table {

    public static GameOverView INSTANCE;
    private final Label summaryLabel;
    private final String summary;
    private TextButton quitButton;
    private Stage parent;

    public GameOverView(Skin skin, final Stage parent) {
        super(skin);
        this.parent = parent;

        setFillParent(true);
        summary = I18N.get("gameOver.text");
        summaryLabel = new Label(summary, UI.SHADE_SKIN, "title");
        summaryLabel.setAlignment(Align.center);
        quitButton = new TextButton(I18N.get("generic.back"), skin);

        add().width(30).expandY();
        add(summaryLabel).colspan(2).expand();
        row();

        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.postRunnable(() -> {
                    GGJ20.INSTANCE.goToMainMenuScreen();
                    GameClientController.getClient().close();
                    GameServerController.getServer().close();
                });
            }
        });
        INSTANCE = this;
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    public void setScore(int score) {
        summaryLabel.setText(summary + "\n" + score);
    }
}

