package org.mssg.ggj20.screens.lobby;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.GFX;
import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.game.Player;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.PlayerCharacterChangedEvent;
import org.mssg.ggj20.network.game.events.PlayerReadyChangeEvent;

import java.util.Map;

public class PlayerSummaryView extends Table {

    public static final String READY_TEXT = I18N.get("lobby.readyButton.readyText");
    public static final String UNREADY_TEXT = I18N.get("lobby.readyButton.unreadyText");
    ;
    public static final int PLAYERS_COUNT = 3;
    public static final String WAITING_FOR_PLAYER = I18N.get("lobby.waitingForPlayer");
    private static PlayerSummaryView THIS;
    public TextButton readyButton;
    private SelectBox characterCombo;
    private LobbyStage lobbyStage;
    private CheckBox[] readyChexkboxes;
    private TextField[] playeNamesTextFields;
    private Image[] playerImages;
    private Image[][] possiblePlayerImages;
    public PlayerSummaryView(Skin skin, LobbyStage lobbyStage) {
        super(skin);
        THIS = this;
        this.lobbyStage = lobbyStage;
        setFillParent(true);
        readyChexkboxes = new CheckBox[PLAYERS_COUNT];
        playeNamesTextFields = new TextField[PLAYERS_COUNT];

        playerImages = new Image[3];
        possiblePlayerImages = new Image[PLAYERS_COUNT][3];

        for (int i = 0; i < PLAYERS_COUNT; i++) {
            readyChexkboxes[i] = new CheckBox("READY", skin);
            readyChexkboxes[i].setDisabled(true);
            playeNamesTextFields[i] = new TextField(WAITING_FOR_PLAYER, skin);
            playeNamesTextFields[i].setDisabled(true);

            possiblePlayerImages[i][0] = new Image(GFX.DONUT_RAPTOR_STILL);
//            possiblePlayerImages[i][0].setScale(13);
            possiblePlayerImages[i][1] = new Image(GFX.OCTO_PIE_STILL);
//            possiblePlayerImages[i][1].setScale(13);
            possiblePlayerImages[i][2] = new Image(GFX.COOKIE_JAR_STILL);
//            possiblePlayerImages[i][2].setScale(13);

            playerImages[i] = new Image(possiblePlayerImages[i][0].getDrawable());
        }

        addPlayersOverview();
        addGameSetup();
    }

    public static final PlayerSummaryView get() {
        return THIS;
    }

    private void addPlayersOverview() {
        for (int i = 0; i < PLAYERS_COUNT; i++) {
            add(playeNamesTextFields[i]).center();
            add(readyChexkboxes[i]).center();
            add().minWidth(30);
            add(playerImages[i]).pad(10).center();
            row();
        }
    }

    private void addGameSetup() {
        characterCombo = new SelectBox(getSkin());
        characterCombo.setItems(PlayerCharacter.DONUT_RAPTOR,
                PlayerCharacter.COOKIE_JAR,
                PlayerCharacter.OCTO_PIE);
        characterCombo.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                PlayerCharacter character = (PlayerCharacter) ((SelectBox) actor).getSelected();

                PlayerCharacterChangedEvent playerStatusEvent = new PlayerCharacterChangedEvent();
                playerStatusEvent.setConnectionId(GameClientController.getGameState().getMyId());
                playerStatusEvent.setCharacter(character);

                GameClientController.getClient().sendTCP(playerStatusEvent);
            }
        });

        readyButton = new TextButton(READY_TEXT, getSkin(), "round");
        readyButton.setUserObject(false);
        readyButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                //TODO sound
//                SoundAssets.get().menuClick.play();
                if ((Boolean) readyButton.getUserObject()) {
                    readyButton.setText(READY_TEXT);
                    readyButton.setUserObject(false);
                    characterCombo.setDisabled(false);
                } else {
                    readyButton.setText(UNREADY_TEXT);
                    readyButton.setUserObject(true);
                    characterCombo.setDisabled(true);

                    PlayerReadyChangeEvent playerReadyChangeEvent = new PlayerReadyChangeEvent();
                    playerReadyChangeEvent.setConnectionId(GameClientController.getGameState().getMyId());
                    playerReadyChangeEvent.setReady((Boolean) readyButton.getUserObject());

                    GameClientController.getClient().sendTCP(playerReadyChangeEvent);
                }
            }
        });

        add(characterCombo).colspan(2);
        add(readyButton).width(160).colspan(2);
        row();
    }

    public void updatePlayerStatus(int index,
                                   String name,
                                   boolean ready,
                                   PlayerCharacter character) {
        if (index >= 3) {
            return;
        }

        playeNamesTextFields[index].setText(name == null ? WAITING_FOR_PLAYER : name);
        readyChexkboxes[index].setChecked(ready);

        if (character != PlayerCharacter.NONE) {
            playerImages[index].setDrawable(possiblePlayerImages[index][character.ordinal()].getDrawable());
            playerImages[index].setVisible(true);
        } else {
            playerImages[index].setVisible(false);
        }

    }

    public void updatePlayerStatus(Map<Integer, Player> playersMap) {
        Player[] players = playersMap.values().toArray(new Player[0]);


        int i = 0;
        for (; i < players.length; i++) {
            Player player = players[i];
            updatePlayerStatus(i,
                    player.getName(),
                    player.isReady(),
                    player.getCharacter());

            if (player.getId() == GameClientController.getGameState().getMyId()) {
                characterCombo.setSelected(player.getCharacter());
            }
        }

        for (; i < PLAYERS_COUNT; i++) {
            updatePlayerStatus(i,
                    WAITING_FOR_PLAYER,
                    false,
                    PlayerCharacter.NONE);
        }

    }
}
