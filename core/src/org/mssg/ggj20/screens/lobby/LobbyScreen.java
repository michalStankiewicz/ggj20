package org.mssg.ggj20.screens.lobby;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

public class LobbyScreen implements Screen {

    private LobbyStage lobbyStage;

    public LobbyScreen() {
        this.lobbyStage = new LobbyStage();
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(lobbyStage);
        lobbyStage.show();
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        lobbyStage.act();
        lobbyStage.getViewport().apply();
        lobbyStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        lobbyStage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
