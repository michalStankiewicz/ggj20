package org.mssg.ggj20.screens.lobby;

import com.badlogic.gdx.scenes.scene2d.Stage;

import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.commons.ui.ChatView;

public class LobbyStage extends Stage {

    private ChatView chatView;
    private PlayerSummaryView playerSummaryView;

    LobbyStage() {
        playerSummaryView = new PlayerSummaryView(UI.SHADE_SKIN, this);
    }

    public void show() {
        chatView = ChatView.getOnStage(this);
        addActor(chatView);
        addActor(playerSummaryView);
        playerSummaryView.readyButton.setText(PlayerSummaryView.READY_TEXT);
    }
}
