package org.mssg.ggj20.screens.lobby;

import org.mssg.ggj20.game.HelpDetails;

public enum PlayerCharacter {
    DONUT_RAPTOR(HelpDetails.DONUT, "Donut raptor"),
    OCTO_PIE(HelpDetails.BUG, "Octo pie"),
    COOKIE_JAR(HelpDetails.COMPUTER, "Cookie jar"),
    NONE(null, "");

    private HelpDetails fixes;
    private String text;

    PlayerCharacter(HelpDetails fixes, String text) {
        this.fixes = fixes;
        this.text = text;
    }

    public boolean canHelpWith(HelpDetails problem) {
        return fixes == problem;
    }

    @Override
    public String toString() {
        return text;
    }
}
