package org.mssg.ggj20.screens.game;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Stage;

import org.mssg.ggj20.commons.ui.ChatView;

public class GameUIStage extends Stage {

    private HUD hud;

    public GameUIStage() {
        hud = HUD.get();

        addActor(hud);
        addActor(ChatView.getOnStage(this));
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (Input.Keys.GRAVE == keyCode) {
            if (getActors().contains(ChatView.get(), true)) {
                ChatView.get().remove();
            } else {
                addActor(ChatView.getOnStage(this));
            }
        }
        return false;
    }
}
