package org.mssg.ggj20.screens.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapRenderer;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.assets.MAPS;
import org.mssg.ggj20.game.GameState;
import org.mssg.ggj20.game.ItGuyEntity;
import org.mssg.ggj20.game.input.InputMonitor;
import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.events.LoadedGameEvent;
import org.mssg.ggj20.screens.game.entities.PlayerEntity;

import java.util.List;

public class GameScreen implements Screen {

    private final OrthographicCamera camera;
    private final InputMultiplexer inputMultiplexer;
    private GameUIStage uiStage;
    private TiledMap map;
    private TiledMapRenderer mapRenderer;

    private Box2DDebugRenderer debugRenderer;
    private SpriteBatch batch;

    private World world;
    private float gameUptime = 0;
    private GameState gameState;
    private PlayerEntity myPlayerEntity;

    public GameScreen() {
        uiStage = new GameUIStage();
        InputMonitor inputMonitor = new InputMonitor();
        debugRenderer = new Box2DDebugRenderer();
        camera = new OrthographicCamera();
        camera.setToOrtho(false, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.update();
        camera.translate(0, 0);
        camera.zoom = 0.02f;
        batch = new SpriteBatch();
        map = MAPS.MAP;
        mapRenderer = new OrthogonalTiledMapRenderer(map, 1.0f / CFG.getInt("tileset.pixPerTile"));

        inputMultiplexer = new InputMultiplexer();
        inputMultiplexer.addProcessor(uiStage);
        inputMultiplexer.addProcessor(inputMonitor);

        world = new World(Vector2.Zero, true);
        PlayerEntity.init();
        ItGuyEntity.init();
    }

    @Override
    public void show() {
        gameState = GameClientController.getGameState();
        gameState.prepareWorld(map, world);

        Gdx.input.setInputProcessor(inputMultiplexer);
        GameClientController.getClient().sendTCP(new LoadedGameEvent());

        myPlayerEntity = GameClientController.getGameState().getMyPlayerEntity();
    }

    @Override
    public void render(float delta) {
        gameUptime += delta;
        Gdx.gl.glClearColor(0, 0, 0.2f, 0);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

//        gameState.updatePositions();
//        gameState.updateItems();
        camera.position.set(myPlayerEntity.getPosition(), camera.position.z);
//
//        viewport.apply();
//        batch.setProjectionMatrix(viewport.getCamera().combined);
//        batch.begin();
//        background.draw(batch);
//        titlebg.draw(batch);
//        batch.end();
        camera.update();

        mapRenderer.setView(camera);
        mapRenderer.render();

        batch.setProjectionMatrix(camera.combined);
        batch.begin();

        renderItGuys(delta, gameUptime, batch, gameState.getItGuys());
        renderPlayers(gameUptime, batch, gameState.getPlayerEntities());

        batch.end();

//        debugRenderer.render(world, camera.combined);

        uiStage.act(delta);
        uiStage.act();
        uiStage.getViewport().apply();
        uiStage.draw();
    }

    private void renderItGuys(float delta, float uptime, SpriteBatch batch, List<ItGuyEntity> itGuys) {
        for (int i = 0; i < itGuys.size(); i++) {
            itGuys.get(i).render(delta, uptime, batch);
        }
    }

    private void renderPlayers(float uptime, SpriteBatch batch, List<PlayerEntity> playerEntities) {
        for (int i = 0; i < playerEntities.size(); i++) {
            playerEntities.get(i).render(uptime, batch, gameState.getItGuysById());
        }
    }

    @Override
    public void resize(int width, int height) {
        uiStage.getViewport().update(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
