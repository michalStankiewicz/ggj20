package org.mssg.ggj20.screens.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Scaling;

import org.mssg.ggj20.assets.GFX;
import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.game.BoardMood;
import org.mssg.ggj20.screens.lobby.PlayerCharacter;

public class HUD extends Table {
    private static final HUD INSTANCE = new HUD();

    private static final Color LOW_RED = new Color(1, 0.8f, 0.8f, 1);
    private static final Color MID_RED = new Color(1, 0.6f, 0.6f, 1);
    private static final Color MORE_RED = new Color(1, 0.3f, 0.3f, 1);

    private final Label infoLabel;
    private final String levelUpText;

    private Label scoreLabel;
    private Label scoreCounter;
    private Label levelLabel;
    private Label levelCounter;

    private Image happy;
    private Image lessHappy;
    private Image worried;
    private Image angry;
    private Image furious;

    private Image boardFace;

    private HUD() {
        super(UI.SHADE_SKIN);
        setFillParent(true);

        levelLabel = new Label(I18N.get("hud.level.text"), UI.SHADE_SKIN, "title");
        levelLabel.setAlignment(Align.right);

        levelCounter = new Label("1", UI.SHADE_SKIN, "title");
        levelCounter.setAlignment(Align.left);

        scoreLabel = new Label(I18N.get("hud.score.text"), UI.SHADE_SKIN, "title");
        scoreLabel.setAlignment(Align.right);

        scoreCounter = new Label("0", UI.SHADE_SKIN, "title");
        scoreCounter.setAlignment(Align.left);

        happy = new Image(GFX.BOARD_HAPPY);
        happy.setScaling(Scaling.fit);
        lessHappy = new Image(GFX.BOARD_LESS_HAPPY);
        lessHappy.setScaling(Scaling.fit);
        worried = new Image(GFX.BOARD_WORRIED);
        worried.setScaling(Scaling.fit);
        angry = new Image(GFX.BOARD_ANGRY);
        angry.setScaling(Scaling.fit);
        furious = new Image(GFX.BOARD_FURIOUS);
        furious.setScaling(Scaling.fit);

        boardFace = happy;
        add(boardFace).prefHeight(100).prefWidth(100).left().top().align(Align.topLeft);

        levelUpText = I18N.get("hud.levelUp");
        infoLabel = new Label(levelUpText, UI.SHADE_SKIN, "title-plain");
        infoLabel.setColor(Color.RED);
        infoLabel.setVisible(false);
        add(infoLabel).expandX().top();

        Table vg = new Table();
        vg.add(scoreLabel);
        vg.add(scoreCounter).width(100);
        vg.row();
        vg.add(levelLabel);
        vg.add(levelCounter).width(100);
        add(vg).minWidth(200).right().align(Align.topRight);
        row();
        add();
        add().expand();
        add();
    }

    public static HUD get() {
        return INSTANCE;
    }

    public void updateMood(BoardMood mood) {
        String text = null;
        switch (mood) {
            case HAPPY:
                boardFace.setDrawable(happy.getDrawable());
                text = I18N.get("board.happyManager");
                break;
            case STILL_OK:
                boardFace.setDrawable(lessHappy.getDrawable());
                text = I18N.get("board.lessHappyManager");
                break;
            case WORRIED:
                boardFace.setDrawable(worried.getDrawable());
                text = I18N.get("board.worriedManager");
                break;
            case ANGRY:
                boardFace.setDrawable(angry.getDrawable());
                text = I18N.get("board.angryManager");
                break;
            case FURIOUS:
                boardFace.setDrawable(furious.getDrawable());
                text = I18N.get("board.furiousManager");
                break;
        }

        displayText(text);
    }

    public void updateScore(int score) {
        INSTANCE.scoreCounter.setText(score);
    }

    public void updateLevel(int level) {
        levelCounter.setText(level);
        updateMood(BoardMood.HAPPY);
        displayText(levelUpText);
    }

    public void displayProblemInfo(PlayerCharacter character) {
        switch (character) {
            case DONUT_RAPTOR:
                displayText(I18N.get("problem.hungryDev"));
                break;
            case COOKIE_JAR:
                displayText(I18N.get("problem.computerBroken"));
                break;
            case OCTO_PIE:
                displayText(I18N.get("problem.bugFound"));
                break;
        }
    }

    private void displayText(String text) {
        infoLabel.clearActions();
        infoLabel.setText(text);
        infoLabel.setVisible(true);
        infoLabel.addAction(new TemporalAction(5) {
            @Override
            protected void update(float percent) {
            }

            @Override
            protected void end() {
                actor.setVisible(false);
                super.end();
            }
        });
    }
}
