package org.mssg.ggj20.screens.game.entities;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

import org.mssg.ggj20.assets.GFX;
import org.mssg.ggj20.game.ItGuyEntity;
import org.mssg.ggj20.screens.lobby.PlayerCharacter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentMap;

public class PlayerEntity {

    public static final BodyDef PLAYER_BODY_DEF = new BodyDef();
    public static final FixtureDef PLAYER_FIXTURE_DEF = new FixtureDef();

    public static final float SPEED = 4;
    public static final float V_DIAGONAL = (float) (Math.sqrt(2) / 2);
    private static final float MAX_LEN = 1000;
    static Vector2 tmp = new Vector2();
    private int ownerId;
    private Body body;
    private Sprite image;
    private Animation<TextureRegion> upAnimation;
    private Animation<TextureRegion> downAnimation;
    private Animation<TextureRegion> leftAnimation;
    private Animation<TextureRegion> rightAnimation;
    private PlayerCharacter character;
    private Integer nearItGuy;
    private boolean up;
    private boolean down;
    private boolean left;
    private boolean right;
    private float animationWidth;
    private float animationHeight;
    private List<RadarMarker> radarMarkers = Collections.synchronizedList(new ArrayList<>());
    private Integer askedToHelp;
    private Integer solvedProblemOfItGuy;

    public static void init() {
        PLAYER_BODY_DEF.type = BodyDef.BodyType.DynamicBody;
        PLAYER_BODY_DEF.active = true;

        PLAYER_FIXTURE_DEF.filter.categoryBits = CollisionType.PLAYER;
        PLAYER_FIXTURE_DEF.filter.maskBits = CollisionType.WALL | CollisionType.IT_GUY;

        CircleShape circle = new CircleShape();
        circle.setRadius(0.6f);
        PLAYER_FIXTURE_DEF.shape = circle;
    }

    public int getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(int ownerId) {
        this.ownerId = ownerId;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public PlayerCharacter getCharacter() {
        return character;
    }

    public void setCharacter(PlayerCharacter character) {
        this.character = character;
        switch (character) {
            case DONUT_RAPTOR:
                this.image = new Sprite(GFX.DONUT_RAPTOR_STILL);
                this.downAnimation = new Animation(0.15f, GFX.RAPTOR_FRONT.getRegions());
                this.leftAnimation = new Animation(0.15f, GFX.RAPTOR_LEFT.getRegions());
                this.rightAnimation = new Animation(0.15f, GFX.RAPTOR_RIGHT.getRegions());
                this.upAnimation = new Animation(0.15f, GFX.RAPTOR_BACK.getRegions());
                break;
            case OCTO_PIE:
                this.image = new Sprite(GFX.OCTO_PIE_STILL);
                this.downAnimation = new Animation(0.15f, GFX.OCTO_FRONT.getRegions());
                this.leftAnimation = new Animation(0.15f, GFX.OCTO_LEFT.getRegions());
                this.rightAnimation = new Animation(0.15f, GFX.OCTO_RIGHT.getRegions());
                this.upAnimation = new Animation(0.15f, GFX.OCTO_BACK.getRegions());
                break;
            case COOKIE_JAR:
                this.image = new Sprite(GFX.COOKIE_JAR_STILL);
                this.downAnimation = new Animation(0.15f, GFX.JAR_FRONT.getRegions());
                this.leftAnimation = new Animation(0.15f, GFX.JAR_LEFT.getRegions());
                this.rightAnimation = new Animation(0.15f, GFX.JAR_RIGHT.getRegions());
                this.upAnimation = new Animation(0.15f, GFX.JAR_BACK.getRegions());
                break;
        }
        image.setScale(0.02f);
        animationWidth = this.downAnimation.getKeyFrame(0).getRegionWidth() * 0.02f;
        animationHeight = this.downAnimation.getKeyFrame(0).getRegionHeight() * 0.02f;
    }

    public void render(float uptime, SpriteBatch batch, ConcurrentMap<Integer, ItGuyEntity> itGuysById) {
        if (left) {
            batch.draw(leftAnimation.getKeyFrame(uptime, true),
                    body.getPosition().x - animationWidth / 2,
                    body.getPosition().y - animationHeight / 2,
                    animationWidth, animationHeight);
        } else if (right) {
            batch.draw(rightAnimation.getKeyFrame(uptime, true),
                    body.getPosition().x - animationWidth / 2,
                    body.getPosition().y - animationHeight / 2,
                    animationWidth, animationHeight);
        } else if (up) {
            batch.draw(upAnimation.getKeyFrame(uptime, true),
                    body.getPosition().x - animationWidth / 2,
                    body.getPosition().y - animationHeight / 2,
                    animationWidth, animationHeight);
        } else if (down) {
            batch.draw(downAnimation.getKeyFrame(uptime, true),
                    body.getPosition().x - animationWidth / 2,
                    body.getPosition().y - animationHeight / 2,
                    animationWidth, animationHeight);
        } else {
            image.draw(batch);
        }

        for (int i = 0; i < radarMarkers.size(); i++) {
            RadarMarker radarMarker = radarMarkers.get(i);
            ItGuyEntity guy = itGuysById.get(radarMarker.getItGuyEntityId());
            tmp.set(guy.getBody().getPosition());
            float len = tmp.sub(body.getPosition()).len();
            if (len > MAX_LEN) {
                len = MAX_LEN;
            }
            float alphaMod = MathUtils.lerp(0, 0.8f, len / MAX_LEN);

            tmp.nor();
            float angle = tmp.angle() - 90;
            radarMarker.render(tmp.add(body.getPosition()),
                    batch,
                    1 - alphaMod,
                    angle);
        }
    }

    public Vector2 getPosition() {
        return body.getPosition();
    }

    public void applyControls(boolean up, boolean down, boolean left, boolean right) {
        float vx = 0;
        float vy = 0;

        if (up) {
            vy += SPEED;
        }

        if (down) {
            vy -= SPEED;
        }

        if (right) {
            vx += SPEED;
        }

        if (left) {
            vx -= SPEED;
        }

        if ((vx * vy) != 0) {
            vx *= V_DIAGONAL;
            vy *= V_DIAGONAL;
        }

        body.setLinearVelocity(vx, vy);
    }

    public void setPosition(float x, float y) {
        body.setTransform(x, y, 0);
        image.setCenter(x, y);
    }

    public Integer getNearItGuy() {
        return nearItGuy;
    }

    public void setNearItGuy(Integer nearItGuy) {
        this.nearItGuy = nearItGuy;
    }

    public void setLastControls(boolean up, boolean down, boolean left, boolean right) {
        this.up = up;
        this.down = down;
        this.left = left;
        this.right = right;
    }

    public void addRadarMarkerFor(ItGuyEntity itGuyEntity) {
        radarMarkers.add(new RadarMarker(itGuyEntity.getId()));
    }

    public void askedToHelp(Integer nearItGuy) {
        askedToHelp = nearItGuy;
    }

    public Integer getAskedToHelp() {
        return askedToHelp;
    }

    public void removeRadar(Integer itGuyId) {
        radarMarkers.removeIf(radarMarker -> radarMarker.getItGuyEntityId() == itGuyId);
    }

    public void advanceRadar(Integer itGuyId) {
        for (int i = 0; i < radarMarkers.size(); i++) {
            if (radarMarkers.get(i).getItGuyEntityId() == itGuyId) {
                radarMarkers.get(i).advance(getCharacter());
                return;
            }
        }
    }

    public void solvedProblem(Integer nearItGuy) {
        this.solvedProblemOfItGuy = nearItGuy;
    }

    public Integer getItGuyWhoIHelped() {
        return solvedProblemOfItGuy;
    }
}
