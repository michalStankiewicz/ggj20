package org.mssg.ggj20.screens.game.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;

class Prop {
    private final int hits;
    private final int points;
    private Sprite sprite;

    public Prop(int hits, int points, Sprite sprite) {
        this.hits = hits;
        this.points = points;
        this.sprite = sprite;
    }

    public int getHits() {
        return hits;
    }

    public int getPoints() {
        return points;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
