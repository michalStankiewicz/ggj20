package org.mssg.ggj20.screens.game.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

import org.mssg.ggj20.assets.GFX;
import org.mssg.ggj20.screens.lobby.PlayerCharacter;

class RadarMarker {
    private int itGuyEntityId;
    private Sprite image;

    public RadarMarker(int itGuyEntityId) {
        this.itGuyEntityId = itGuyEntityId;
        image = new Sprite(GFX.HELP_GENERIC_POINTER);
        image.setScale(0.02f);

    }

    public int getItGuyEntityId() {
        return itGuyEntityId;
    }

    public void render(Vector2 position, SpriteBatch batch, float alpha, float angle) {
        image.setCenter(position.x, position.y);
        image.setOriginCenter();
        image.setRotation(angle);
        image.setAlpha(alpha);
        image.draw(batch);
    }

    public void advance(PlayerCharacter character) {
        switch (character) {
            case DONUT_RAPTOR:
                image.setTexture(GFX.RAPTOR_NEEDED_POINTER);
                break;
            case OCTO_PIE:
                image.setTexture(GFX.OCTO_NEEDED_POINTER);
                break;
            case COOKIE_JAR:
                image.setTexture(GFX.JAR_NEEDED_POINTER);
                break;
        }
    }
}
