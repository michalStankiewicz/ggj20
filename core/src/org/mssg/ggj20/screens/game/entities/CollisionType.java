package org.mssg.ggj20.screens.game.entities;

public interface CollisionType {
    short WALL = 1;
    short IT_GUY = 2;
    short PLAYER = 4;
}
