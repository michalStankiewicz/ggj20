package org.mssg.ggj20.screens.game.entities;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

public class PropEntity {

    private static final BodyDef ITEM_BODY_DEF = new BodyDef();
    private static final FixtureDef ITEM_FIXTURE_DEF = new FixtureDef();
    private static final float SHAKE_TIMEOUT = 3;

    private Sprite image;
    private Body body;
    private Fixture fixture;

    private int hp;
    private float shakeTimeoutLeft = -1;
    private boolean touchedByGhost;
    private int ghostId;
    private boolean dead = false;
    private int id;

    public static PropEntity create(int id, MapObject item, World world) {
        String type = (String) item.getProperties().get("type");

        float x = (float) item.getProperties().get("x") / 16;
        float y = (float) item.getProperties().get("y") / 16;
        float width = (float) item.getProperties().get("width") / 16;
        float height = (float) item.getProperties().get("height") / 16;

        ITEM_BODY_DEF.position.x = 2 * x + width;
        ITEM_BODY_DEF.position.y = 2 * y + height;
        ITEM_BODY_DEF.type = BodyDef.BodyType.StaticBody;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(width, height);
        ITEM_FIXTURE_DEF.shape = shape;

//        ITEM_FIXTURE_DEF.filter.categoryBits = CollisionType.ITEM;
//        ITEM_FIXTURE_DEF.filter.maskBits = CollisionType.GHOST;
        ITEM_FIXTURE_DEF.isSensor = true;

        PropEntity propEntity = new PropEntity();
        propEntity.id = id;
        propEntity.body = world.createBody(ITEM_BODY_DEF);
        propEntity.fixture = propEntity.body.createFixture(ITEM_FIXTURE_DEF);
//        Prop properties = GameProps.props.get(type);

//        propEntity.hp = properties.getHits();

//        propEntity.image = new Sprite(properties.getSprite());
//        propEntity.image.setScale(1f / 8);
//        propEntity.image.setCenter(propEntity.body.getPosition().x,
//                propEntity.body.getPosition().y);

        propEntity.fixture.setUserData(propEntity);

        return propEntity;
    }

    public Sprite getImage() {
        return image;
    }

    public Body getBody() {
        return body;
    }

    public Fixture getFixture() {
        return fixture;
    }

    public void draw(float delta, SpriteBatch batch) {
        if (dead) {
            return;
        }
//        if (GameClientController.get().getGameState().getMyTeam() == PlayerEntity.Team.GHOSTS) {
//            image.setColor(Color.PINK);
//        } else {
//            image.setColor(Color.WHITE);
//        }

        if (shakeTimeoutLeft > 0) {
            float dx = (float) (Math.random() - 0.5f) * shakeTimeoutLeft * 0.2f;
            float dy = (float) (Math.random() - 0.5f) * shakeTimeoutLeft * 0.2f;
            image.setCenter(body.getPosition().x + dx,
                    body.getPosition().y + dy);
            shakeTimeoutLeft -= delta;
        } else {
            image.setCenter(body.getPosition().x,
                    body.getPosition().y);
        }

        image.draw(batch);
    }

    public void setTouchedByGhost(boolean touchedByGhost, int ghostId) {
        this.ghostId = ghostId;
        this.touchedByGhost = touchedByGhost;
        if (touchedByGhost) {
            shakeTimeoutLeft = SHAKE_TIMEOUT;
        } else {
            shakeTimeoutLeft = -1;
        }
    }

    public float getShakeTimeoutLeft() {
        return shakeTimeoutLeft;
    }

    public boolean getTouchedByGhost() {
        return touchedByGhost;
    }

    public void decreaseTouched(float delta) {
        if (shakeTimeoutLeft > 0) {
            shakeTimeoutLeft -= delta;
        } else {
            touchedByGhost = false;
            ghostId = -1;
        }
    }

    public void setShakeTimeOut(float timeout) {
        shakeTimeoutLeft = timeout;
    }

    public int getGhostId() {
        return ghostId;
    }

    public void seGhostId(int ghostId) {
        this.ghostId = ghostId;
    }

    public boolean hit() {
        System.out.println(hp);
        if (--hp <= 0) {
            return true;
        }
        return false;
    }

    public void markDead() {
        dead = true;
    }

    public boolean isDead() {
        return dead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
