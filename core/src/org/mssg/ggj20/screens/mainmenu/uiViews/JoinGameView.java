package org.mssg.ggj20.screens.mainmenu.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.network.connector.ConnectorClientController;
import org.mssg.ggj20.network.connector.objects.ConnectionInitEvent;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

public class JoinGameView extends Table {

    private Label ipLabel;
    private TextField ipTextField;
    private Label portLabel;
    private TextField portTextField;

    private Label userNameLabel;
    private TextField userNameTextField;

    private Label passwordLabel;
    private TextField passwordField;

    private TextButton joinGameButton;

    private TextButton returnButton;

    public JoinGameView(Skin skin, MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        ipLabel = new Label(I18N.get("multiplayer.ip.text"), skin, "subtitle");
        ipTextField = new TextField("127.0.0.1", skin);
        portLabel = new Label(I18N.get("multiplayer.port.text"), skin, "subtitle");
        portTextField = new TextField("12345", skin);
        userNameLabel = new Label(I18N.get("multiplayer.username.text"), skin, "subtitle");
        userNameTextField = new TextField("", skin);
        passwordLabel = new Label(I18N.get("multiplayer.password.text"), skin, "subtitle");
        passwordField = new TextField("", skin);
        passwordField.setPasswordMode(true);
        passwordField.setPasswordCharacter('*');
        joinGameButton = new TextButton(I18N.get("multiplayer.joinButton.text"), skin);
        returnButton = new TextButton(I18N.get("generic.back"), skin);

        buildUI();

        addListeners(menuStage);
        validateJoinGameSettings();
    }

    private void buildUI() {
        add().width(30).expandY();
        add().colspan(2).expand();
        row();

        addField(ipLabel, ipTextField);
        addField(portLabel, portTextField);
        addField(userNameLabel, userNameTextField);
        addField(passwordLabel, passwordField);

        add().width(30).height(10);
        add().height(10).colspan(2).expandX();
        row();

        addMenuButton(joinGameButton);
        addMenuButton(returnButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
    }

    private void addField(Label label, TextField field) {
        add().width(30);

        add(label).width(200).left();
        add(field).width(150).expandX().padLeft(10).left();

        row();
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    private void addListeners(final MenuStage menuStage) {
        userNameTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateJoinGameSettings();
            }
        });

        ipTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateJoinGameSettings();
            }
        });

        portTextField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateJoinGameSettings();
            }
        });

        returnButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

        joinGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                ConnectorClientController.join(
                        ipTextField.getText(),
                        Integer.parseInt(portTextField.getText()),
                        new ConnectionInitEvent(
                                userNameTextField.getText(),
                                passwordField.getText(),
                                ipTextField.getText()));
            }
        });
    }

    private void validateJoinGameSettings() {
        if (userNameTextField.getText() == null ||
                userNameTextField.getText().equals("")) {
            joinGameButton.setTouchable(Touchable.disabled);
            joinGameButton.setVisible(false);
            return;
        }

        if (ipTextField.getText() == null ||
                ipTextField.getText().equals("") ||
                !ipTextField.getText().matches("^\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}$")) {
            joinGameButton.setTouchable(Touchable.disabled);
            joinGameButton.setVisible(false);
            return;
        }

        if (portTextField.getText() == null ||
                portTextField.getText().equals("") ||
                !portTextField.getText().matches("^\\d*$")) {
            joinGameButton.setTouchable(Touchable.disabled);
            joinGameButton.setVisible(false);
            return;
        }

        joinGameButton.setTouchable(Touchable.enabled);
        joinGameButton.setVisible(true);
    }
}
