package org.mssg.ggj20.screens.mainmenu.uiViews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.CFG;
import org.mssg.ggj20.assets.GFX;
import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

public class TutorialView extends Table {

    private Image tutorial;
    private TextButton quitButton;


    public TutorialView(Skin skin, final MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        //TODO tutorial screenshot
        tutorial = new Image();
        tutorial = new Image(GFX.TUTORIAL);
        tutorial.setSize(tutorial.getWidth() * CFG.getFloat("tutorialImage.scale"),
                tutorial.getHeight() * CFG.getFloat("tutorialImage.scale"));
        quitButton = new TextButton(I18N.get("generic.back"), skin);

        add().width(30).expandY();
        add(tutorial).colspan(2).expand();
        row();

        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }
}
