package org.mssg.ggj20.screens.mainmenu.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.network.NetworkController;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

public class HostGameView extends Table {

    private Label userNameLabel;
    private TextField userNameField;

    private Label passwordLabel;
    private TextField passwordField;

    private TextButton startGameButton;
    private TextButton returnButton;

    public HostGameView(Skin skin, MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        userNameLabel = new Label(I18N.get("multiplayer.username.text"), skin, "subtitle");
        userNameField = new TextField("player", skin);
        passwordLabel = new Label(I18N.get("multiplayer.password.text"), skin, "subtitle");
        passwordField = new TextField("", skin);
        passwordField.setPasswordMode(true);
        passwordField.setPasswordCharacter('*');
        startGameButton = new TextButton(I18N.get("multiplayer.startButton.text"), skin);
        returnButton = new TextButton(I18N.get("generic.back"), skin);

        buildUI();

        addListeners(menuStage);
        validateHostSettings();
    }

    private void buildUI() {
        add().width(30).expandY();
        add().colspan(2).expand();
        row();

        addField(userNameLabel, userNameField);
        addField(passwordLabel, passwordField);

        add().width(30).height(10);
        add().height(10).colspan(2).expandX();
        row();

        addMenuButton(startGameButton);
        addMenuButton(returnButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
    }

    private void addField(Label label, TextField field) {
        add().width(30);

        add(label).width(200).left();
        add(field).width(150).expandX().padLeft(10).left();

        row();
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    private void addListeners(final MenuStage menuStage) {
        returnButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

        userNameField.addListener(new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                validateHostSettings();
            }
        });

        startGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                NetworkController.hostGame(passwordField.getText());
                NetworkController.joinLocal(userNameField.getText(),
                        passwordField.getText());
                startGameButton.setTouchable(Touchable.disabled);

                startGameButton.addAction(new TemporalAction(3) {
                    @Override
                    protected void update(float percent) {
                    }

                    @Override
                    protected void end() {
                        actor.setTouchable(Touchable.enabled);
                        super.end();
                    }
                });
            }
        });
    }

    private void validateHostSettings() {
        if (userNameField.getText() == null ||
                userNameField.getText().equals("")) {
            startGameButton.setTouchable(Touchable.disabled);
            startGameButton.setVisible(false);
            return;
        }

        startGameButton.setTouchable(Touchable.enabled);
        startGameButton.setVisible(true);
    }
}
