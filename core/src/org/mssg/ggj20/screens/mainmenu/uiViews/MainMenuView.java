package org.mssg.ggj20.screens.mainmenu.uiViews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.screens.mainmenu.MenuStage;
import org.mssg.ggj20.screens.mainmenu.actions.GameMenuAction;

public class MainMenuView extends Table {

    private TextButton singlePlayerGameButton;
    private TextButton joinGameButton;
    private TextButton hostGameButton;
    private TextButton tutorialButton;
    private TextButton optionsButton;
    private TextButton creditsButton;
    private TextButton quitButton;


    public MainMenuView(Skin skin, final MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        joinGameButton = new TextButton(I18N.get("mainMenu.joinGameButton.text"), skin);
        hostGameButton = new TextButton(I18N.get("mainMenu.hostGameButton.text"), skin);
        singlePlayerGameButton = new TextButton(I18N.get("mainMenu.localGameButton.text"), skin);
        optionsButton = new TextButton(I18N.get("mainMenu.optionsButton.text"), skin);
        quitButton = new TextButton(I18N.get("mainMenu.quitButton.text"), skin);
        tutorialButton = new TextButton(I18N.get("mainMenu.tutorialButton.text"), skin);
        creditsButton = new TextButton(I18N.get("mainMenu.creditsButton.text"), skin);

        add().width(30).expandY();
        add().expand();
        row();

//        addMenuButton(singlePlayerGameButton);
        addMenuButton(joinGameButton);
        addMenuButton(hostGameButton);
        addMenuButton(tutorialButton);
        addMenuButton(optionsButton);
        addMenuButton(creditsButton);
        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).expandX();

        hostGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToHostGameView();
            }
        });

        joinGameButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToJoinGameView();
            }
        });

        tutorialButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToTutorialView();
            }
        });

        optionsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToOptionsView();
            }
        });

        creditsButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToCreditsView();
            }
        });

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.confirmAction(I18N.get("confirmExitGame.text"),
                        GameMenuAction.QUIT_GAME_ACTION);
            }
        });
    }


    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).expandX().left();
        row();
    }
}
