package org.mssg.ggj20.screens.mainmenu.uiViews;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.SND;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

public class OptionsView extends Table {

    private TextButton returnButton;
    private Label soundLabel;
    private CheckBox soundCheckbox;
    private Label musicLabel;
    private CheckBox musicCheckbox;

    public OptionsView(Skin skin, MenuStage menuStage) {
        super(skin);

        setFillParent(true);

        soundLabel = new Label(I18N.get("options.sound.text"), skin);
        soundCheckbox = new CheckBox(null, skin, "switch");
        soundCheckbox.setChecked(true);

        soundCheckbox.addListener(event -> {
            if (musicCheckbox.isChecked()) {
                SND.on();
            } else {
                SND.off();
            }

            return true;
        });

        musicLabel = new Label(I18N.get("options.music.text"), skin);
        musicCheckbox = new CheckBox(null, skin, "switch");
        musicCheckbox.setChecked(true);

        musicCheckbox.addListener(event -> {
            if (musicCheckbox.isChecked()) {
                SND.MUSIC.setVolume(1.0f);
            } else {
                SND.MUSIC.setVolume(0.0f);
            }

            return true;
        });

        musicCheckbox.setChecked(false);

        returnButton = new TextButton(I18N.get("generic.back"), skin);

        buildUI();

        addListeners(menuStage);
    }

    private void buildUI() {
        add().width(30).expandY();
        add().colspan(2).expand();
        row();

        addField(soundLabel, soundCheckbox);
        addField(musicLabel, musicCheckbox);

        add().width(30).height(100);
        add().height(100).colspan(2).expandX();
        row();


        addMenuButton(returnButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();
    }

    private void addField(Label label, Actor field) {
        add().width(30);

        add(label).width(200).left();
        add(field).width(150).expandX().padLeft(10).left();

        row();
    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }

    private void addListeners(final MenuStage menuStage) {
        returnButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });
    }
}
