package org.mssg.ggj20.screens.mainmenu.uiViews;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.screens.mainmenu.MenuStage;

public class CreditsView extends Table {
    private final Label creditsLabel;
    private TextButton quitButton;
    private MenuStage menuStage;

    public CreditsView(Skin skin, final MenuStage menuStage) {
        super(skin);
        this.menuStage = menuStage;

        setFillParent(true);
        String text = "GRAPHICS:\n" +
                "Marta Mieczkowska\n" +
                "Ada Jozwik\n" +
                "PROGRAMMING:\n" +
                "Michal Stankiewicz\n" +
                "MUSIC:\n" +
                "Song: Vexento - Pixel Party (NoCopyrightMusic - FreeToUse)\n" +
                "Promoted by Amazing Music.\n" +
                "Original Video: https://youtu.be/W579qTUmH5k\n" +
                "SOUND EFFECTS:\n" +
                "\"Robotic Sound FX\" by Ekuhvielle\n" +
                "https://freesound.org/people/Ekuhvielle/sounds/211071/\n";
        creditsLabel = new Label(text, UI.SHADE_SKIN, "title");
        quitButton = new TextButton(I18N.get("generic.back"), skin);

        add().width(30).expandY();
        add(creditsLabel).colspan(2).expand();
        row();

        addMenuButton(quitButton);

        add().width(30).height(30);
        add().height(30).colspan(2).expandX();

        quitButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                menuStage.switchToMainMenu();
            }
        });

    }

    private void addMenuButton(TextButton button) {
        add().width(30).height(30);
        add(button).height(30).width(150).colspan(2).left();
        row();
    }
}

