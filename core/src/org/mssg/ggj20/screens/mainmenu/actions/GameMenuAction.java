package org.mssg.ggj20.screens.mainmenu.actions;

@FunctionalInterface
public interface GameMenuAction {

    GameMenuAction QUIT_GAME_ACTION = new QuitGameAction();
    GameMenuAction NOP_ACTION = () -> {
    };
    GameMenuAction LEAVE_GAME_ACTION = new LeaveGameAction();
    GameMenuAction UNHOST_GAME_ACTION = new UnhostGameAction();

    void doAction();

}
