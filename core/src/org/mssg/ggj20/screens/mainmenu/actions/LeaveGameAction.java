package org.mssg.ggj20.screens.mainmenu.actions;

import com.badlogic.gdx.Gdx;

import org.mssg.ggj20.GGJ20;
import org.mssg.ggj20.network.game.GameClientController;

class LeaveGameAction implements GameMenuAction {
    @Override
    public void doAction() {
        Gdx.app.postRunnable(() -> GGJ20.INSTANCE.goToMainMenuScreen());
        GameClientController.getClient().close();
    }
}
