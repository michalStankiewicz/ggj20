package org.mssg.ggj20.screens.mainmenu.actions;

import com.badlogic.gdx.Gdx;

class QuitGameAction implements GameMenuAction {
    @Override
    public void doAction() {
        Gdx.app.exit();
        System.exit(0);
    }
}
