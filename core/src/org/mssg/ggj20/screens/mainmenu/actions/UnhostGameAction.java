package org.mssg.ggj20.screens.mainmenu.actions;

import org.mssg.ggj20.network.game.GameClientController;
import org.mssg.ggj20.network.game.GameServerController;

class UnhostGameAction implements GameMenuAction {
    @Override
    public void doAction() {
        GameServerController.getGameState().reset();
        GameClientController.getGameState().reset();
        GameClientController.getTickCounter().stop();
        GameServerController.getTickCounter().stop();
        GameServerController.getServer().close();
    }
}
