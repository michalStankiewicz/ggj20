package org.mssg.ggj20.screens.mainmenu;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import org.mssg.ggj20.assets.I18N;
import org.mssg.ggj20.assets.UI;
import org.mssg.ggj20.commons.ui.DialogWindow;
import org.mssg.ggj20.screens.mainmenu.actions.GameMenuAction;
import org.mssg.ggj20.screens.mainmenu.uiViews.CreditsView;
import org.mssg.ggj20.screens.mainmenu.uiViews.HostGameView;
import org.mssg.ggj20.screens.mainmenu.uiViews.JoinGameView;
import org.mssg.ggj20.screens.mainmenu.uiViews.MainMenuView;
import org.mssg.ggj20.screens.mainmenu.uiViews.OptionsView;
import org.mssg.ggj20.screens.mainmenu.uiViews.TutorialView;

public class MenuStage extends Stage {

    private static MenuStage THIS;
    private MainMenuView mainMenuView;
    private JoinGameView joinGameView;
    private HostGameView hostGameView;
    private CreditsView creditsView;
    private TutorialView tutorialView;
    private OptionsView optionsView;

    MenuStage() {
        THIS = this;
        mainMenuView = new MainMenuView(UI.SHADE_SKIN, this);
        joinGameView = new JoinGameView(UI.SHADE_SKIN, this);
        hostGameView = new HostGameView(UI.SHADE_SKIN, this);
        creditsView = new CreditsView(UI.SHADE_SKIN, this);
        tutorialView = new TutorialView(UI.SHADE_SKIN, this);
        optionsView = new OptionsView(UI.SHADE_SKIN, this);

        addActor(mainMenuView);
        addActor(joinGameView);
        addActor(hostGameView);
        addActor(creditsView);
        addActor(tutorialView);
        addActor(optionsView);

        switchToMainMenu();
    }

    public static MenuStage getInstance() {
        return THIS;
    }

    @Override
    public boolean keyDown(int keyCode) {
        if (Input.Keys.ESCAPE == keyCode && mainMenuView.isVisible()) {
            confirmAction(I18N.get("confirmExitGame.text"),
                    GameMenuAction.QUIT_GAME_ACTION);
        }
        return true;
    }

    public void switchToMainMenu() {
        switchTo(mainMenuView);
    }

    public void switchToHostGameView() {
        switchTo(hostGameView);
    }

    public void switchToJoinGameView() {
        switchTo(joinGameView);
    }

    public void switchToTutorialView() {
        switchTo(tutorialView);
    }

    public void switchToCreditsView() {
        switchTo(creditsView);
    }

    public void switchToOptionsView() {
        switchTo(optionsView);
    }

    private void hideAllViews() {
        for (int i = 0; i < getActors().size; i++) {
            getActors().items[i].setVisible(false);
        }
    }

    public void showError(String error) {
        getActors().get(0).setTouchable(Touchable.disabled);
        DialogWindow.getErrorDialog(error, GameMenuAction.NOP_ACTION).show(this);
    }

    public void confirmAction(String question, GameMenuAction action) {
        getActors().get(0).setTouchable(Touchable.disabled);
        DialogWindow.getConfirmActionWindow(question, action).show(this);
    }

    private void switchTo(Table menuView) {
        hideAllViews();
        menuView.setTouchable(Touchable.enabled);
        menuView.setVisible(true);
    }
}
