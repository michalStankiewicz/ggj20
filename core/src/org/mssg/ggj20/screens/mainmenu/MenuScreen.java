package org.mssg.ggj20.screens.mainmenu;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FillViewport;

import org.mssg.ggj20.assets.GFX;

public class MenuScreen implements Screen {

    private final SpriteBatch batch;
    private final Sprite background;
    private final OrthographicCamera camera;
    private final FillViewport viewport;
    private MenuStage menuStage;

    public MenuScreen() {
        menuStage = new MenuStage();

        batch = new SpriteBatch();
        background = new Sprite(GFX.BACKGROUND);
        background.setScale(4);
        background.setCenter(0,0);
        camera = new OrthographicCamera();
        viewport = new FillViewport(Gdx.graphics.getWidth(),
                Gdx.graphics.getHeight(), camera);
    }

    @Override
    public void show() {
        Gdx.input.setInputProcessor(menuStage);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        viewport.apply();
        batch.setProjectionMatrix(viewport.getCamera().combined);
        batch.begin();
        background.draw(batch);
        batch.end();

        menuStage.act();
        menuStage.getViewport().apply();
        menuStage.draw();
    }

    @Override
    public void resize(int width, int height) {
        menuStage.getViewport().update(width, height, true);
        viewport.update(width, height, false);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        menuStage.dispose();
    }
}
